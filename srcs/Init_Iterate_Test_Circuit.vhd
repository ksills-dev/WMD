library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

library std;
use std.textio.all;

library unisim;
use unisim.vcomponents.all;

use work.common.all;
use work.hw_info.all;
use work.matrices.all;
use work.ccc.all;

entity Init_Iterate_Test_Circuit is
  port (
    master_clock          : in  Std_Logic;
    
    board_button_center   : in  Std_Logic;
    board_button_left     : in  Std_Logic;
    board_button_right    : in  Std_Logic;
    
    board_switch          : in  Std_Logic_Vector(15 downto 0);
    
    board_led             : out Std_Logic_Vector(15 downto 0);
    board_display_segment : out Std_Logic_Vector(6 downto 0);
    board_display_digit   : out Std_Logic_Vector(3 downto 0);
    board_display_decimal : out Std_Logic
  );
end entity Init_Iterate_Test_Circuit;

architecture Behavioral of Init_Iterate_Test_Circuit is

  constant matrix_desc : Matrix := (
    n_rows    => 3,
    n_columns => 3,
    bitwidth  => Width_8
  );
  constant data_file : String := "input.hex";

  signal matrix_request  : Matrix_Access_Request ( index (
      row    (addressing_size(matrix_desc.n_rows)    - 1 downto 0),
      column (addressing_size(matrix_desc.n_columns) - 1 downto 0)));
  signal matrix_data_out : Unsigned((as_natural(matrix_desc.bitwidth) - 1) downto 0);

  signal step_signal : Std_Logic;
  signal bcd         : Std_Logic_Vector(11 downto 0);
begin
  matrix : entity work.Matrix_Accessor(Dataflow)
    generic map(
      description => matrix_desc,
      block_count => 1,
      init_file   => data_file
    )
    port map(
      clock           => master_clock,
      port_a_request  => matrix_request,
      port_a_data_in  => to_unsigned(0, as_natural(matrix_desc.bitwidth)),
      port_a_data_out => matrix_data_out,
      port_b_request  => (No_Op, (to_unsigned(0, addressing_size(matrix_desc.n_rows)), to_unsigned(0, addressing_size(matrix_desc.n_columns)))),
      port_b_data_in  => to_unsigned(0, as_natural(matrix_desc.bitwidth)),
      port_b_data_out => open
    );

  iterator : entity work.Matrix_BCD_Iterator(Behavioral)
    generic map(
      matrix_desc => matrix_desc
    )
    port map(
      clock => master_clock,

      matrix_accessor_request  => matrix_request,
      matrix_accessor_data_out => matrix_data_out,
      take_step                => step_signal,

      element_available   => board_led(15),
      element_ends_row    => board_led(14),
      element_ends_matrix => board_led(13),
      element_as_bcd      => bcd
    );

  board_led(11 downto 0) <= bcd;

  step_clock : entity work.Clock_Divider(Behavioral)
    generic map(
      freq_in  => master_clock_freq, 
      freq_out => 1
    )
    port map(
      reset     => '0',
      clock_in  => master_clock,
      clock_out => step_signal
    );

  display_driver : entity work.CCC_Board_Display_Driver(Behavioral)
    port map(
      master_clock => master_clock,
      message(3)   => to_ccchar('-'),
      message(2)   => bcd_to_ccchar(bcd(11 downto 8)),
      message(1)   => bcd_to_ccchar(bcd(7 downto 4)),
      message(0)   => bcd_to_ccchar(bcd(3 downto 0)),
      decimals     => B"0000",

      board_display_segment => board_display_segment,
      board_display_digit   => board_display_digit,
      board_display_decimal => board_display_decimal
    );
end architecture Behavioral;