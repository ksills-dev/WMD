----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/14/2018 12:11:53 PM
-- Design Name: 
-- Module Name: binary_to_ascii - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity binary_to_ascii_tx is
  Port (
    clock :              in STD_LOGIC;
    binary_in :          in UNSIGNED (19 downto 0); --rename BCD_in
    new_data_signal :    in STD_LOGIC;
    element_ends_row :   in STD_LOGIC;
    rx_full:             in STD_LOGIC;
    tx_fifo_not_empty :  in STD_LOGIC;
    ascii_out :          out UNSIGNED (7 downto 0);  
    tx_done_tick :       out STD_LOGIC;--;
    ready_for_input :    out STD_LOGIC
 );
 
end binary_to_ascii_tx;

architecture Behavioral of binary_to_ascii_tx is

--component binary_to_bcd
--   Port ( 
--        binary_in : in UNSIGNED (15 downto 0);
--        bcd_out :   out UNSIGNED (19 downto 0)
--   );
--end component;

component bcd_to_ascii
    generic (
        N : integer
    );
    Port ( 
        clock :              in STD_LOGIC;
        element_ends_row:    in STD_LOGIC;
        rx_full :            in STD_LOGIC;
        bcd_in :             in UNSIGNED (19 downto 0);  --Largest is 2^16 -1 = 65535, 5 digits each 4 bits
        new_data_signal :    in STD_LOGIC;
        ascii_out :          out UNSIGNED (7 downto 0);  
        tx_done_tick :       out STD_LOGIC;--;
        ready_for_input :    out STD_LOGIC
    );
end component;

--signal bcd_wire : UNSIGNED (19 downto 0);

begin
 
--binary_to_bcd_unit : binary_to_bcd
--    port map(
--        binary_in   => binary_in,
--        bcd_out     => bcd_wire
--    );
    
bcd_to_ascii_unit : bcd_to_ascii
    generic map(
        N => 20
    )
    port map(
        clock               =>  clock,
        new_data_signal     =>  new_data_signal,
        element_ends_row    =>  element_ends_row,
        rx_full             =>  rx_full,
        bcd_in              =>  binary_in, --rename BCD in
        ascii_out           =>  ascii_out,
        tx_done_tick        =>  tx_done_tick,
        ready_for_input     =>  ready_for_input
    );

end Behavioral;
