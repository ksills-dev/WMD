library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.common.all;
use work.matrices.all;
use work.hw_info.all;
use work.ccc.all;

entity Adder_Test_Circuit is
  port (
    master_clock          : in  Std_Logic;
    
    board_button_center   : in  Std_Logic;
    board_button_left     : in  Std_Logic;
    board_button_right    : in  Std_Logic;
    
    board_switch          : in  Std_Logic_Vector(15 downto 0);
    
    board_led             : out Std_Logic_Vector(15 downto 0);
    board_display_segment : out Std_Logic_Vector(6 downto 0);
    board_display_digit   : out Std_Logic_Vector(3 downto 0);
    board_display_decimal : out Std_Logic
  
  );
end entity Adder_Test_Circuit;

architecture Behavioral of Adder_Test_Circuit is
  constant input_matrix : Matrix := (
    n_rows    => 4,
    n_columns => 4,
    bitwidth  => Width_8 
  );  
  constant output_matrix : Matrix := (
    n_rows    => input_matrix.n_rows,
    n_columns => input_matrix.n_columns,
    bitwidth  => Width_16 
  );
  constant row_index_length    : Natural := addressing_size(input_matrix.n_rows);
  constant column_index_length : Natural := addressing_size(input_matrix.n_columns);

  -- Matrix A Accessor
  signal matrix_a_port_a_request  : Matrix_Access_Request ( index (
                                      row    ((   row_index_length - 1) downto 0),
                                      column ((column_index_length - 1) downto 0)));
  signal matrix_a_port_b_request  : Matrix_Access_Request ( index (
                                      row    ((   row_index_length - 1) downto 0),
                                      column ((column_index_length - 1) downto 0)));
  --signal matrix_a_port_a_data_in  : Unsigned(7 downto 0);
  --signal matrix_a_port_b_data_in  : Unsigned(7 downto 0);
  signal matrix_a_port_a_data_out : Unsigned(7 downto 0);
  signal matrix_a_port_b_data_out : Unsigned(7 downto 0);
  
  -- Matrix B Accessor
  signal matrix_b_port_a_request  : Matrix_Access_Request ( index (
                                      row    ((   row_index_length - 1) downto 0),
                                      column ((column_index_length - 1) downto 0)));
  signal matrix_b_port_b_request  : Matrix_Access_Request ( index (
                                      row    ((   row_index_length - 1) downto 0),
                                      column ((column_index_length - 1) downto 0)));
  --signal matrix_b_port_a_data_in  : Unsigned(7 downto 0);
  --signal matrix_b_port_b_data_in  : Unsigned(7 downto 0);
  signal matrix_b_port_a_data_out : Unsigned(7 downto 0);
  signal matrix_b_port_b_data_out : Unsigned(7 downto 0);
  
  -- Matrix C Accessor
  signal matrix_c_port_a_request  : Matrix_Access_Request ( index (
                                      row    ((   row_index_length - 1) downto 0),
                                      column ((column_index_length - 1) downto 0)));
  signal matrix_c_port_b_request  : Matrix_Access_Request ( index (
                                      row    ((   row_index_length - 1) downto 0),
                                      column ((column_index_length - 1) downto 0)));
  signal matrix_c_port_a_data_in  : Unsigned(15 downto 0);
  signal matrix_c_port_b_data_in  : Unsigned(15 downto 0);
  signal matrix_c_port_a_data_out : Unsigned(15 downto 0);
  signal matrix_c_port_b_data_out : Unsigned(15 downto 0);
  
  -- Adder
  signal adder_input_aa_request  : Matrix_Access_Request ( index (
                                    row    ((   row_index_length - 1) downto 0),
                                    column ((column_index_length - 1) downto 0)));
  signal adder_input_ab_request  : Matrix_Access_Request ( index (
                                    row    ((   row_index_length - 1) downto 0),
                                    column ((column_index_length - 1) downto 0)));
  
  signal adder_input_ba_request  : Matrix_Access_Request ( index (
                                    row    ((   row_index_length - 1) downto 0),
                                    column ((column_index_length - 1) downto 0)));
  signal adder_input_bb_request  : Matrix_Access_Request ( index (
                                    row    ((   row_index_length - 1) downto 0),
                                    column ((column_index_length - 1) downto 0)));
                                   
  signal adder_output_a_request  : Matrix_Access_Request ( index (
                                    row    ((   row_index_length - 1) downto 0),
                                    column ((column_index_length - 1) downto 0)));
  signal adder_output_a_data_out   : Unsigned(15 downto 0);
  signal adder_output_b_request  : Matrix_Access_Request ( index (
                                    row    ((   row_index_length - 1) downto 0),
                                    column ((column_index_length - 1) downto 0)));
  
  -- Controller State
  signal working        : Std_Logic := '0';
  signal adder_complete : Std_Logic;
  signal button_is_held : Std_Logic := '0';
   
  -- Iterator
  signal step_signal      : Std_Logic := '0';
  signal iterator_request : Matrix_Access_Request ( index (
                              row    ((   row_index_length - 1) downto 0),
                              column ((column_index_length - 1) downto 0)));
  signal iterator_data_in : Unsigned(15 downto 0);
  
  -- Output
  signal result_as_bcd   : Std_Logic_Vector(19 downto 0);
  signal display_message : CCCtring(3 downto 0);
  
begin
  matrix_a : entity work.Matrix_Accessor(Dataflow)
    generic map (
      description => input_matrix,
      block_count => 1,
      init_file  => "_test_input_1.hex" 
    )
    port map (
      clock => master_clock,
      
      port_a_request              => matrix_a_port_a_request,
      port_a_data_in (7 downto 0) => Unsigned(board_switch(15 downto 8)),
      port_a_data_out(7 downto 0) => matrix_a_port_a_data_out,
      
      port_b_request              => matrix_a_port_b_request,
      port_b_data_in (7 downto 0) => B"00000000",
      port_b_data_out(7 downto 0) => matrix_a_port_b_data_out
    );
    
  matrix_b : entity work.Matrix_Accessor(Dataflow)
    generic map (
      description => input_matrix,
      block_count => 1,
      init_file  => "_test_input_2.hex"
    )
    port map (
      clock => master_clock,
      
      port_a_request              => matrix_b_port_a_request,
      port_a_data_in (7 downto 0) => Unsigned(board_switch(7 downto 0)),
      port_a_data_out(7 downto 0) => matrix_b_port_a_data_out,
      
      port_b_request              => matrix_b_port_b_request,
      port_b_data_in (7 downto 0) => B"00000000",
      port_b_data_out(7 downto 0) => matrix_b_port_b_data_out
    );
    
  matrix_c : entity work.Matrix_Accessor(Dataflow)
    generic map (
      description => output_matrix,
      block_count => 2
    )
    port map (
      clock => master_clock,
      
      port_a_request  => matrix_c_port_a_request,
      port_a_data_in  => matrix_c_port_a_data_in,
      port_a_data_out => matrix_c_port_a_data_out,
      
      port_b_request  => matrix_c_port_b_request,
      port_b_data_in  => matrix_c_port_b_data_in,
      port_b_data_out => matrix_c_port_b_data_out
    );

  adder_unit : entity work.Adder(Linear)
    generic map(
      matrix_a => input_matrix,
      matrix_b => input_matrix,
      matrix_c => output_matrix
    )
    port map(
      clock  => master_clock,
      enable => working,
      
      matrix_a_accessor_port_a_request  => adder_input_aa_request,
      matrix_a_accessor_port_a_data_out => matrix_a_port_a_data_out,
      matrix_a_accessor_port_b_request  => adder_input_ab_request,
      matrix_a_accessor_port_b_data_out => matrix_a_port_b_data_out,
      
      matrix_b_accessor_port_a_request  => adder_input_ba_request,
      matrix_b_accessor_port_a_data_out => matrix_b_port_a_data_out,
      matrix_b_accessor_port_b_request  => adder_input_bb_request,
      matrix_b_accessor_port_b_data_out => matrix_b_port_b_data_out,
      
      matrix_c_accessor_port_a_request => adder_output_a_request,
      matrix_c_accessor_port_a_data_in => matrix_c_port_a_data_in,
      matrix_c_accessor_port_b_request => adder_output_b_request,
      matrix_c_accessor_port_b_data_in => matrix_c_port_b_data_in,
      
      is_complete => adder_complete      
    );
    
  step_clock : entity work.Clock_Divider(Behavioral)
    generic map(
      freq_in  => master_clock_freq,
      freq_out => 1 
    )
    port map(
      reset     => '0',
      clock_in  => master_clock,
      clock_out => step_signal 
    );
    
  iterator : entity work.Matrix_BCD_Iterator(Behavioral)
    generic map (
      matrix_desc => output_matrix
    )
    port map(
      clock => master_clock,
      
      matrix_accessor_request  => iterator_request,
      matrix_accessor_data_out => iterator_data_in,
      
      take_step           => step_signal,
      element_ends_row    => board_led(15),
      element_ends_matrix => board_led(14),
      element_as_bcd      => result_as_bcd
    );
    
  display_driver : entity work.CCC_Board_Display_Driver(Behavioral)
    port map (
      master_clock          => master_clock,
      message               => display_message,
      decimals(3)           => button_is_held,
      decimals(2)           => adder_complete,
      decimals(1 downto 0)  => B"00",
      board_display_segment => board_display_segment,
      board_display_digit   => board_display_digit,
      board_display_decimal => board_display_decimal
    );  

  matrix_a_port_a_request <= adder_input_aa_request;
  matrix_a_port_b_request <= adder_input_ab_request;
  matrix_b_port_a_request <= adder_input_ba_request;
  matrix_b_port_b_request <= adder_input_bb_request;
                                
  matrix_c_port_a_request  <= adder_output_a_request when adder_complete = '0' else iterator_request;
  matrix_c_port_b_request  <= adder_output_b_request;
  
  iterator_data_in <= matrix_c_port_a_data_out when adder_complete = '1' else to_unsigned(0, 16);
  
  board_led(7 downto 0) <= Std_logic_Vector(matrix_c_port_a_data_out(7 downto 0));

  process (master_clock)
  begin
    if rising_edge(master_clock) then
      if board_button_center = '1' then
        button_is_held <= '1';
        
        if button_is_held = '1' then
          working <= '0';
        else
          working <= '1';
        end if;
      else
        working        <= '0';
        button_is_held <= '0';
      end if;
    
      if adder_complete = '1' then
        display_message(3) <= bcd_to_ccchar(result_as_bcd(15 downto 12));
        display_message(2) <= bcd_to_ccchar(result_as_bcd(11 downto 8));
        display_message(1) <= bcd_to_ccchar(result_as_bcd(7  downto 4));
        display_message(0) <= bcd_to_ccchar(result_as_bcd(3  downto 0));      
      else        
        display_message <= to_ccctring("hold");
      end if;
    end if;
  end process;
end architecture Behavioral;