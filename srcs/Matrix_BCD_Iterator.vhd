library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

use work.common.all;
use work.matrices.all;
use work.hw_info.all;

--------------------------------------------------------------------------------
-- Matrix BCD Iterator
------------------
-- As a note, triggering the `take_step` via a high signal prior to the prior 
-- element being available *will* result in iteration and the next 
-- `element_available` signal will be the desired element and not that element
-- previously requested.
--------------------------------------------------------------------------------
entity Matrix_BCD_Iterator is
  generic (
    matrix_desc : Matrix
  );
  port (
    clock : in Std_Logic;

    matrix_accessor_request  : out Matrix_Access_Request ( index (
                                    row    ((addressing_size(matrix_desc.n_rows)    - 1) downto 0),
                                    column ((addressing_size(matrix_desc.n_columns) - 1) downto 0)));
    matrix_accessor_data_out : in  Unsigned((as_natural(matrix_desc.bitwidth) - 1) downto 0);

    take_step : in Std_Logic;
    
    element_available   : out Std_Logic;
    element_ends_row    : out Std_Logic;
    element_ends_matrix : out Std_Logic;
    element_as_bcd      : out Std_Logic_Vector(
                            (4 * Integer(ceil(log10(2.0 ** as_natural(matrix_desc.bitwidth)))) - 1) 
                            downto 0)
  );
end entity Matrix_BCD_Iterator;

architecture Behavioral of Matrix_BCD_Iterator is
  signal index : Matrix_Index (
      row    (matrix_accessor_request.index.row'range),
      column (matrix_accessor_request.index.column'range)
    ) := (
      row    => to_unsigned(matrix_desc.n_rows    - 1, addressing_size(matrix_desc.n_rows)),
      column => to_unsigned(matrix_desc.n_columns - 1, addressing_size(matrix_desc.n_columns))
    );
  signal next_index : Matrix_Index (
    row    (matrix_accessor_request.index.row'range),
    column (matrix_accessor_request.index.column'range));
    
  signal complete      : Std_Logic := '1';
  signal next_complete : Std_Logic;
  signal bcd_complete  : Std_Logic;

begin
  matrix_accessor_request.operator <= Element_Read when take_step = '1' else No_Op;

  element_ends_row    <= '1' 
                           when index.column = (matrix_desc.n_columns - 1) 
                           else 
                         '0';
  element_ends_matrix <= '1' 
                           when (element_ends_row = '1') and 
                                (index.row = (matrix_desc.n_rows - 1)) 
                           else 
                         '0';

  next_index.row    <= to_unsigned(0, next_index.row'high + 1) 
                         when (element_ends_matrix = '1') else
                       index.row + 1
                         when (element_ends_matrix = '0') and
                              (element_ends_row    = '1')
                         else 
                       index.row;
  next_index.column <= to_unsigned(0, next_index.column'high + 1) 
                         when (element_ends_row = '1')
                         else 
                       index.column + 1; 

  matrix_accessor_request.index <= next_index when take_step = '1' else index;


  encoder : entity work.Binary_BCD_Encoder(Behavioral)
    generic map (
      binary_length => as_natural(matrix_desc.bitwidth),
      bcd_length    => element_as_bcd'high + 1
    )
    port map (
      clock       => clock,
      binary      => Std_Logic_Vector(matrix_accessor_data_out),
      bcd         => element_as_bcd,
      is_complete => bcd_complete
    );

  next_complete <= '1' when bcd_complete = '1' and (complete = '0' or take_step = '0') else '0';
  
  element_available <= next_complete;

  process (clock)
  begin
    if rising_edge(clock) then
      if take_step = '1' and complete = '1' then
        index <= next_index;
      end if;
      
      complete <= next_complete;
    end if;
  end process;
end architecture Behavioral;