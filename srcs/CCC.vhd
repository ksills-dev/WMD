library IEEE;
use IEEE.Std_Logic_1164.all;
use IEEE.Numeric_Std.all;

--------------------------------------------------------------------------------
-- **DISCLAIMER**
-----------------
-- The CCC package and CCC_Board_Display_Driver are very much works in progress.
-- As written, they were hastily written to meet the demands of the current
-- assignment and as such are subject to arbitrary changes in functionality.
-- End-user be aware, proceed at your own risk.
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- C.ool
-- C.haracter
-- C.et
-----------------
-- This package (and the subsequent CCC_Board_Display_Driver) offer an extended
-- means of driving the Basys3 Artix 7 FPGA board's 7-segment display.
-- 
-- The character set is designed to provide as much of the ASCII alphanumeric
-- set as possible while avoiding characters that cannot be displayed reasonably
-- on the provided hardware. Also included are several symbols that may be found
-- useful.
--
-- For a list of provided characters, see `ccc-set.txt`.
--
-- The package includes functions to convert a character or string to it's
-- representation in the CCC, returning nicely typed CCChar or CCCtring values.
-- Unfortunately, not all values provided can be represented this way and will
-- need to be manually programmed
--------------------------------------------------------------------------------
package CCC is
  subtype CCChar is Std_Logic_Vector(5 downto 0);
  type CCCtring is array (Natural range <>) of CCChar;

  function bcd_to_ccchar(bcd : in Std_Logic_Vector(3 downto 0)) return CCChar;
  function to_ccchar(value : in Character) return CCChar;
  function to_ccctring(value : in String) return CCCtring;
end package CCC;
 
package body CCC is
  function bcd_to_ccchar(bcd : in Std_Logic_Vector(3 downto 0)) return CCChar is
    variable result : Std_Logic_Vector(5 downto 0);
  begin
    result := Std_Logic_Vector(to_unsigned(0, 2)) & bcd;
    return CCChar(result);
  end bcd_to_ccchar;

  function to_ccchar(value : in Character) return CCChar is
    variable result : CCChar;
  begin
    case value is
      when '0' 
         | 'D'
         | 'O' => result := Std_Logic_Vector(to_unsigned(0, 6));
      when '1' => result := Std_Logic_Vector(to_unsigned(1, 6));
      when '2' => result := Std_Logic_Vector(to_unsigned(2, 6));
      when '3' => result := Std_Logic_Vector(to_unsigned(3, 6));
      when '4' => result := Std_Logic_Vector(to_unsigned(4, 6));
      when '5' 
         | 'S' => result := Std_Logic_Vector(to_unsigned(5, 6));
      when '6' => result := Std_Logic_Vector(to_unsigned(6, 6));
      when '7' => result := Std_Logic_Vector(to_unsigned(7, 6));
      when '8'
         | 'B' => result := Std_Logic_Vector(to_unsigned(8, 6));
      when '9' => result := Std_Logic_Vector(to_unsigned(9, 6));
      when 'A' => result := Std_Logic_Vector(to_unsigned(10, 6));
      when 'a'
         | 'o' => result := Std_Logic_Vector(to_unsigned(11, 6));
      when 'b' => result := Std_Logic_Vector(to_unsigned(12, 6));
      when 'C' 
         | '['
         | '(' => result := Std_Logic_Vector(to_unsigned(13, 6));
      when 'c' => result := Std_Logic_Vector(to_unsigned(14, 6));
      when 'd' => result := Std_Logic_Vector(to_unsigned(15, 6));
      when 'E' => result := Std_Logic_Vector(to_unsigned(16, 6));
      when 'e' => result := Std_Logic_Vector(to_unsigned(17, 6));
      when 'F' => result := Std_Logic_Vector(to_unsigned(18, 6));
      when 'G' => result := Std_Logic_Vector(to_unsigned(19, 6));
      when 'H' 
         | 'X' => result := Std_Logic_Vector(to_unsigned(20, 6));
      when 'h' => result := Std_Logic_Vector(to_unsigned(21, 6));
      when 'I' 
         | 'l' => result := Std_Logic_Vector(to_unsigned(22, 6));
      when 'i' => result := Std_Logic_Vector(to_unsigned(23, 6));
      when 'J' => result := Std_Logic_Vector(to_unsigned(24, 6));
      when 'L' => result := Std_Logic_Vector(to_unsigned(25, 6));
      when 'N' => result := Std_Logic_Vector(to_unsigned(26, 6));
      when 'n' => result := Std_Logic_Vector(to_unsigned(27, 6));
      when 'P' => result := Std_Logic_Vector(to_unsigned(28, 6));
      when 'R' => result := Std_Logic_Vector(to_unsigned(29, 6));
      when 'r' => result := Std_Logic_Vector(to_unsigned(30, 6));
      when 't' => result := Std_Logic_Vector(to_unsigned(31, 6));
      when 'U' 
         | 'V' => result := Std_Logic_Vector(to_unsigned(32, 6));
      when 'u' 
         | 'v' => result := Std_Logic_Vector(to_unsigned(33, 6));
      when 'y' => result := Std_Logic_Vector(to_unsigned(34, 6));
      --when '' => result := Std_Logic_Vector(to_unsigned(35, 6)); -- TODO
      when '?' => result := Std_Logic_Vector(to_unsigned(36, 6));
      when '>' => result := Std_Logic_Vector(to_unsigned(37, 6));
      when '<' => result := Std_Logic_Vector(to_unsigned(38, 6));
      when '^' => result := Std_Logic_Vector(to_unsigned(39, 6));
      when '=' => result := Std_Logic_Vector(to_unsigned(40, 6)); -- High equal
      when '`' => result := Std_Logic_Vector(to_unsigned(41, 6));
      when ''' => result := Std_Logic_Vector(to_unsigned(42, 6));
      --when '' => result := Std_Logic_Vector(to_unsigned(43, 6)); -- TODO
      when ')'
         | ']' => result := Std_Logic_Vector(to_unsigned(44, 6));
      when '-' => result := Std_Logic_Vector(to_unsigned(45, 6));
      when '_' => result := Std_Logic_Vector(to_unsigned(46, 6));
      when '*' => result := Std_Logic_Vector(to_unsigned(47, 6));
      when ' ' => result := Std_Logic_Vector(to_unsigned(48, 6));
      --when '┌' => result := Std_Logic_Vector(to_unsigned(49, 6));
      --when '�?' => result := Std_Logic_Vector(to_unsigned(50, 6));
      --when '└' => result := Std_Logic_Vector(to_unsigned(51, 6));
      --when '┘' => result := Std_Logic_Vector(to_unsigned(52, 6));
      --when '├' => result := Std_Logic_Vector(to_unsigned(53, 6));
      --when '┤' => result := Std_Logic_Vector(to_unsigned(54, 6));
      when ',' => result := Std_Logic_Vector(to_unsigned(55, 6)); -- Bottom Left
      when ':' => result := Std_Logic_Vector(to_unsigned(56, 6)); -- Low Equal
      --when '▔' => result := Std_Logic_Vector(to_unsigned(57, 6));
      --when '║' => result := Std_Logic_Vector(to_unsigned(58, 6));
      --when '�?' => result := Std_Logic_Vector(to_unsigned(59, 6)); -- Top and bottom
      -- when '╙' => result := Std_Logic_Vector(to_unsigned(60, 6)); -- Top left and right
      -- when '╓' => result := Std_Logic_Vector(to_unsigned(61, 6)); -- Bot left and right
      when '/' => result := Std_Logic_Vector(to_unsigned(62, 6));
      when '\' => result := Std_Logic_Vector(to_unsigned(63, 6));
      when others =>
        Report "Non-CCC character found."
        Severity Failure;
    end case;
    return result;
  end to_ccchar;

  function to_ccctring(value : in String) return CCCtring is
    variable result : CCCtring(value'range);
  begin
    for i in value'range loop
      result(i) := to_ccchar(value(i));
    end loop;
    return result;
  end to_ccctring;
end package body CCC;