library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.matrices.all;
use work.common.all;

entity Controller is
  port (
    master_clock          : in  Std_Logic;
    
    board_button_center   : in  Std_Logic;
    board_button_left     : in  Std_Logic;
    board_button_right    : in  Std_Logic;
    board_switch          : in  Std_Logic_Vector(15 downto 0);
    board_led             : out Std_Logic_Vector(15 downto 0);
    board_display_segment : out Std_Logic_Vector(7 downto 0);
    board_display_decimal : out Std_Logic
    
    -- Will need UART
  );
end entity Controller;

architecture Dataflow of Controller is
  type Operation is (Operation_Add, Operation_Multiply);
  signal operator : Operation := Operation_Add;

  signal adder_enable       : Std_Logic := '0';
  signal multiplier_enable  : Std_Logic := '0';
  signal operation_complete : Std_Logic := '1';

  type State is (State_Idle, State_Calculate, State_Transmit);
  signal current_state : State := State_Idle;
  
begin

  -- Instantiate Matrix Accessor A
  -- Instantiate Matrix Accessor B
  -- Instantiate Matrix Accessor C
  
  -- Instantiate Adder
  -- Instantiate Multiplier
  
  process (master_clock)
  begin
    if rising_edge(master_clock) then
      if current_state = State_Calculate then      
        if operation_complete = '1' then
          current_State <= State_Transmit;
          adder_enable      <= '0';
          multiplier_enable <= '0';
        else
          if operator = Operation_Add then
            adder_enable      <= '1';
            multiplier_enable <= '0';
          else
            adder_enable      <= '0';
            multiplier_enable <= '1';
          end if;
        end if;
        
      elsif current_State = State_Transmit then
        -- TODO: Do transmission stuff      
      else
        if board_button_left = '1' then
          operator <= Operation_Add;
        elsif board_button_right = '1' then
          operator <= Operation_Multiply;
        else
          operator <= operator; 
        end if;
        
        if board_button_center = '1' then
          if operator = Operation_Add then
            adder_enable      <= '1';
            multiplier_enable <= '0';
          else
            adder_enable      <= '0';
            multiplier_enable <= '1';
          end if;
        else
          adder_enable      <= '0';
          multiplier_enable <= '0';
        end if;
      end if;
    end if;
  end process;
  
  -- Instantiate CCC_Display_Driver
  -- Instantiate UART Driver
end architecture Dataflow;