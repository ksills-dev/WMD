library IEEE;
use IEEE.Std_Logic_1164.all;
use IEEE.Numeric_Std.all;

use work.hw_info.all;
use work.ccc.all;

entity CCC_Board_Display_Driver is
  Port    ( master_clock          : in  Std_Logic;
            message               : in  CCCtring(3 downto 0);
            decimals              : in  Std_Logic_Vector(3 downto 0) := B"0000";
            board_display_segment : out Std_Logic_Vector(6 downto 0);
            board_display_digit   : out Std_Logic_Vector(3 downto 0);
            board_display_decimal : out Std_Logic);
end CCC_Board_Display_Driver;

architecture Behavioral of CCC_Board_Display_Driver is
  signal driver_clock  : Std_Logic;    
  signal current_digit : Std_Logic_Vector(1 downto 0);
begin
  contrained_clock : entity work.Clock_Divider (Behavioral)
    Generic map (freq_in   => master_clock_freq,
                 freq_out  => sseg_display_freq)
    Port    map (reset     => '0',
                 clock_in  => master_clock,
                 clock_out => driver_clock);

  process (driver_clock)
    function ccchar_to_segments(char : CCChar) 
                                return Std_Logic_Vector is
      variable result : Std_Logic_Vector(6 downto 0);
      variable temp : Std_Logic_Vector(5 downto 0) := char;
    begin
      case temp is
        when B"000000" => result :=  B"1000000";
        when B"000001" => result :=  B"1001111";
        when B"000010" => result :=  B"0100100";
        when B"000011" => result :=  B"0110000";
        when B"000100" => result :=  B"0011001";
        when B"000101" => result :=  B"0010010";
        when B"000110" => result :=  B"0000010";
        when B"000111" => result :=  B"1111000";
        when B"001000" => result :=  B"0000000";
        when B"001001" => result :=  B"0010000";
        when B"001010" => result :=  B"0001000";
        when B"001011" => result :=  B"0100011";
        when B"001100" => result :=  B"0000011";
        when B"001101" => result :=  B"1000110";
        when B"001110" => result :=  B"0100111";
        when B"001111" => result :=  B"0100001";
        when B"010000" => result :=  B"0000110";
        when B"010001" => result :=  B"0000100";
        when B"010010" => result :=  B"0001110";
        when B"010011" => result :=  B"1000010";
        when B"010100" => result :=  B"0001001";
        when B"010101" => result :=  B"0001011";
        when B"010110" => result :=  B"1111001";
        when B"010111" => result :=  B"1111011";
        when B"011000" => result :=  B"1100001";
        when B"011001" => result :=  B"1000111";
        when B"011010" => result :=  B"1001000";
        when B"011011" => result :=  B"0101011";
        when B"011100" => result :=  B"0001100";
        when B"011101" => result :=  B"1001100";
        when B"011110" => result :=  B"0101111";
        when B"011111" => result :=  B"0000111";
        when B"100000" => result :=  B"1000001";
        when B"100001" => result :=  B"1100011";
        when B"100010" => result :=  B"0010001";
        when B"100011" => result :=  B"1111111"; -- TODO: UNDEFINED
        when B"100100" => result :=  B"0101100";
        when B"100101" => result :=  B"0111100";
        when B"100110" => result :=  B"0011110";
        when B"100111" => result :=  B"1011100";
        when B"101000" => result :=  B"0111110";
        when B"101001" => result :=  B"1111101";
        when B"101010" => result :=  B"1011111";
        when B"101011" => result :=  B"1111111"; -- TODO: UNDEFINED
        when B"101100" => result :=  B"1110000";
        when B"101101" => result :=  B"0111111";
        when B"101110" => result :=  B"1110111";
        when B"101111" => result :=  B"0011100";
        when B"110000" => result :=  B"1111111";
        when B"110001" => result :=  B"1011110";
        when B"110010" => result :=  B"1111100";
        when B"110011" => result :=  B"1100111";
        when B"110100" => result :=  B"1110011";
        when B"110101" => result :=  B"0001111";
        when B"110110" => result :=  B"0111001";
        when B"110111" => result :=  B"1101111";
        when B"111000" => result :=  B"0110111";
        when B"111001" => result :=  B"1111110";
        when B"111010" => result :=  B"1001001";
        when B"111011" => result :=  B"1110110";
        when B"111100" => result :=  B"1011101";
        when B"111101" => result :=  B"1101011";
        when B"111110" => result :=  B"0101101";
        when B"111111" => result :=  B"0011011";
        when others => 
          report "UNREACHABLE" severity failure; 
          return B"1111111"; -- Is this a good failure state?
      end case;
      return result;
    end ccchar_to_segments;
    
  begin
    if rising_edge(driver_clock) then
      case current_digit is
        when B"00" => 
          current_digit <= B"01";
          board_display_digit   <= B"1110";
          board_display_segment <= ccchar_to_segments(message(0));
          board_display_decimal <= not decimals(0); 
        when B"01" => 
          current_digit <= B"10";
          board_display_digit   <= B"1101";
          board_display_segment <= ccchar_to_segments(message(1));
          board_display_decimal <= not decimals(1);
        when B"10" => 
          current_digit <= B"11";
          board_display_digit   <= B"1011";
          board_display_segment <= ccchar_to_segments(message(2));
          board_display_decimal <= not decimals(2);
        when B"11" => 
          current_digit <= B"00";
          board_display_digit   <= B"0111";
          board_display_segment <= ccchar_to_segments(message(3));
          board_display_decimal <= not decimals(3);
        when others =>
          report "UNREACHABLE" severity failure;
      end case;
    end if;
  end process;
end Behavioral;