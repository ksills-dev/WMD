library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.common.all;
use work.matrices.all;
use work.hw_info.all;
use work.ccc.all;

entity matrix_operations_top is
    Port (
        master_clock          : in  Std_Logic;
        
        board_button_top      : in  Std_Logic;
        board_button_center   : in  Std_Logic;
        board_button_left     : in  Std_Logic;
        board_button_right    : in  Std_Logic;
        board_button_bottom   : in  Std_Logic;
        board_switch          : in  Std_Logic_Vector(15 downto 0);

        board_led             : out Std_Logic_Vector(15 downto 0);
        board_display_segment : out Std_Logic_Vector(6 downto 0);
        board_display_digit   : out Std_Logic_Vector(3 downto 0);
        board_display_decimal : out Std_Logic;
        tx                    : out Std_Logic  --Serial out from FPGA
   
   );
end matrix_operations_top;

architecture Behavioral of matrix_operations_top is

signal UART_ready_for_input : std_logic;
signal print_data_signal : std_logic;
signal new_data_signal : std_logic;

    constant input_matrix : Matrix := (
        n_rows    => 4,
        n_columns => 4,
        bitwidth  => Width_8 
    );  
  
    constant output_matrix : Matrix := (
        n_rows    => input_matrix.n_rows,
        n_columns => input_matrix.n_columns,
        bitwidth  => Width_16 
    );
    
    constant    row_index_length : Natural := addressing_size(input_matrix.n_rows);
    constant column_index_length : Natural := addressing_size(input_matrix.n_columns);

    -- Matrix A Accessor
    signal matrix_a_port_a_request  : Matrix_Access_Request ( index (
                                        row    ((   row_index_length - 1) downto 0),
                                        column ((column_index_length - 1) downto 0)));
    signal matrix_a_port_b_request  : Matrix_Access_Request ( index (
                                        row    ((   row_index_length - 1) downto 0),
                                        column ((column_index_length - 1) downto 0)));
    signal matrix_a_port_a_data_out : Unsigned(7 downto 0);
    signal matrix_a_port_b_data_out : Unsigned(7 downto 0);
    
    -- Matrix B Accessor
    signal matrix_b_port_a_request  : Matrix_Access_Request ( index (
                                        row    ((   row_index_length - 1) downto 0),
                                        column ((column_index_length - 1) downto 0)));
    signal matrix_b_port_b_request  : Matrix_Access_Request ( index (
                                        row    ((   row_index_length - 1) downto 0),
                                        column ((column_index_length - 1) downto 0)));
    signal matrix_b_port_a_data_out : Unsigned(7 downto 0);
    signal matrix_b_port_b_data_out : Unsigned(7 downto 0);
    
    -- Matrix C Accessor
    signal matrix_c_port_a_request  : Matrix_Access_Request ( index (
                                        row    ((   row_index_length - 1) downto 0),
                                        column ((column_index_length - 1) downto 0)));
    signal matrix_c_port_b_request  : Matrix_Access_Request ( index (
                                        row    ((   row_index_length - 1) downto 0),
                                        column ((column_index_length - 1) downto 0)));
    signal matrix_c_port_a_data_in  : Unsigned(15 downto 0);
    signal matrix_c_port_b_data_in  : Unsigned(15 downto 0);
    signal matrix_c_port_a_data_out : Unsigned(15 downto 0);
    signal matrix_c_port_b_data_out : Unsigned(15 downto 0);

    --Adder
    signal adder_input_aa_request  : Matrix_Access_Request ( index (
                                      row    ((   row_index_length - 1) downto 0),
                                      column ((column_index_length - 1) downto 0)));
    signal adder_input_ab_request  : Matrix_Access_Request ( index (
                                      row    ((   row_index_length - 1) downto 0),
                                      column ((column_index_length - 1) downto 0)));
    signal adder_input_aa_data_out : Unsigned(as_natural(input_matrix.bitwidth) - 1 downto 0);
    signal adder_input_ab_data_out : Unsigned(as_natural(input_matrix.bitwidth) - 1 downto 0);
    
    signal adder_input_ba_request  : Matrix_Access_Request ( index (
                                      row    ((   row_index_length - 1) downto 0),
                                      column ((column_index_length - 1) downto 0)));
    signal adder_input_bb_request  : Matrix_Access_Request ( index (
                                      row    ((   row_index_length - 1) downto 0),
                                      column ((column_index_length - 1) downto 0)));
    signal adder_input_ba_data_out : Unsigned(as_natural(input_matrix.bitwidth) - 1 downto 0);
    signal adder_input_bb_data_out : Unsigned(as_natural(input_matrix.bitwidth) - 1 downto 0);
    

    signal adder_output_a_request  : Matrix_Access_Request ( index (
                                      row    ((   row_index_length - 1) downto 0),
                                      column ((column_index_length - 1) downto 0)));
    signal adder_output_b_request  : Matrix_Access_Request ( index (
                                      row    ((   row_index_length - 1) downto 0),
                                      column ((column_index_length - 1) downto 0)));
    signal adder_output_a_data_in : Unsigned(as_natural(output_matrix.bitwidth) - 1 downto 0);
    signal adder_output_b_data_in : Unsigned(as_natural(output_matrix.bitwidth) - 1 downto 0);
        
    -- Multiplier
    signal multiplier_input_a_request : Matrix_Access_Request ( index (
                                          row    ((   row_index_length - 1) downto 0),
                                          column ((column_index_length - 1) downto 0)));
    signal multiplier_input_b_request : Matrix_Access_Request ( index (
                                          row    ((   row_index_length - 1) downto 0),
                                          column ((column_index_length - 1) downto 0)));
    signal multiplier_input_a_data_out : Unsigned(as_natural(input_matrix.bitwidth) - 1 downto 0);
    signal multiplier_input_b_data_out : Unsigned(as_natural(input_matrix.bitwidth) - 1 downto 0);
                                     
    signal multiplier_output_request  : Matrix_Access_Request ( index (
                                          row    ((   row_index_length - 1) downto 0),
                                          column ((column_index_length - 1) downto 0)));
    signal multiplier_output_data_in  : Unsigned(15 downto 0);
    
    -- Controller State
    type Calculation_Type is (Matrix_Add, Matrix_Multiply);
    signal operation    : Calculation_Type;

    type Controller_State is (State_Idle, State_Calculating, State_Transmitting);
    signal current_state  : Controller_State := State_Idle;
    
    signal      adder_enable : Std_Logic := '0';
    signal multiplier_enable : Std_Logic := '0';

    signal      adder_complete : Std_Logic;
    signal multiplier_complete : Std_Logic;

    -- Input
    signal debounced_button_center : Std_Logic;
    signal button_is_held          : Std_Logic := '0';
        
    -- Iterator
    signal iterator_step_signal : Std_Logic := '0';
    signal iterator_request     : Matrix_Access_Request ( index (
                                    row    ((   row_index_length - 1) downto 0),
                                    column ((column_index_length - 1) downto 0)));
    signal iterator_data_in     : Unsigned(15 downto 0);

    signal iterator_element_available   : Std_Logic;
    signal iterator_element_as_bcd      : Std_Logic_Vector(19 downto 0);
    signal iterator_element_ends_row    : Std_Logic;
    signal iterator_element_ends_matrix : Std_Logic;
    
    -- Output
    signal display_message : CCCtring(3 downto 0);

begin

  ------------------------------------------------------------------------------
  -- Child Entity Instantiation
  matrix_a : entity work.Matrix_Accessor(Dataflow)
    generic map (
      description => input_matrix,
      block_count => 1,
      init_file  => "_test_input_1.hex" 
    )
    port map (
      clock => master_clock,
      
      port_a_request  => matrix_a_port_a_request,
      port_a_data_in  => B"00000000",
      port_a_data_out => matrix_a_port_a_data_out,
      
      port_b_request  => matrix_a_port_b_request,
      port_b_data_in  => B"00000000",
      port_b_data_out => matrix_a_port_b_data_out
    );
    
  matrix_b : entity work.Matrix_Accessor(Dataflow)
    generic map (
      description => input_matrix,
      block_count => 1,
      init_file  => "_test_input_2.hex"
    )
    port map (
      clock => master_clock,
      
      port_a_request  => matrix_b_port_a_request,
      port_a_data_in  => B"00000000",
      port_a_data_out => matrix_b_port_a_data_out,
      
      port_b_request  => matrix_b_port_b_request,
      port_b_data_in  => B"00000000",
      port_b_data_out => matrix_b_port_b_data_out
    );
    
  matrix_c : entity work.Matrix_Accessor(Dataflow)
    generic map (
      description => output_matrix,
      block_count => 2
    )
    port map (
      clock => master_clock,
      
      port_a_request  => matrix_c_port_a_request,
      port_a_data_in  => matrix_c_port_a_data_in,
      port_a_data_out => matrix_c_port_a_data_out,
      
      port_b_request  => matrix_c_port_b_request,
      port_b_data_in  => matrix_c_port_b_data_in,
      port_b_data_out => matrix_c_port_b_data_out
    );

  adder_unit : entity work.Adder(Linear)
    generic map(
      matrix_a => input_matrix,
      matrix_b => input_matrix,
      matrix_c => output_matrix
    )
    port map(
      clock  => master_clock,
      enable => adder_enable,
      
      matrix_a_accessor_port_a_request  => adder_input_aa_request,
      matrix_a_accessor_port_a_data_out => adder_input_aa_data_out,
      matrix_a_accessor_port_b_request  => adder_input_ab_request,
      matrix_a_accessor_port_b_data_out => adder_input_ab_data_out,
      
      matrix_b_accessor_port_a_request  => adder_input_ba_request,
      matrix_b_accessor_port_a_data_out => adder_input_ba_data_out,
      matrix_b_accessor_port_b_request  => adder_input_bb_request,
      matrix_b_accessor_port_b_data_out => adder_input_bb_data_out,
      
      matrix_c_accessor_port_a_request => adder_output_a_request,
      matrix_c_accessor_port_a_data_in => adder_output_a_data_in,
      matrix_c_accessor_port_b_request => adder_output_b_request,
      matrix_c_accessor_port_b_data_in => adder_output_b_data_in,
      
      is_complete => adder_complete  
    );

  multiplier_unit : entity work.Matrix_Multiplier(Linear)
    generic map(
      matrix_a => input_matrix,
      matrix_b => input_matrix,
      matrix_c => output_matrix
    )
    port map(
      clock  => master_clock,
      enable => multiplier_enable,
      
      matrix_a_request  => multiplier_input_a_request,
      matrix_a_data_out => multiplier_input_a_data_out,
      
      matrix_b_request  => multiplier_input_b_request,
      matrix_b_data_out => multiplier_input_b_data_out,
      
      matrix_c_request => multiplier_output_request,
      matrix_c_data_in => multiplier_output_data_in,
      
      is_complete => multiplier_complete      
    );
    
    

    
  iterator : entity work.Matrix_BCD_Iterator(Behavioral)
    generic map (
      matrix_desc => output_matrix
    )
    port map(
      clock => master_clock,
      
      matrix_accessor_request  => iterator_request,
      matrix_accessor_data_out => iterator_data_in,
      
      take_step           => iterator_step_signal,
      element_available   => iterator_element_available,
      element_ends_row    => iterator_element_ends_row,
      element_ends_matrix => iterator_element_ends_matrix,
      element_as_bcd      => iterator_element_as_bcd
    );
  
  board_led (4) <= iterator_element_ends_matrix;  
  board_led (5) <= iterator_step_signal;
  board_led (6) <= print_data_signal;
  board_led (7) <= UART_ready_for_input;
  board_led (8) <= board_button_center;
  board_led (9) <= debounced_button_center;
  board_led (10) <= button_is_held;
    
  UART : entity work. uart_top
    port map(
        tx                => tx,       
        clock             => master_clock,            
        reset             => '0', 
        print_data_signal => print_data_signal,    --500/s
        new_data_signal   => new_data_signal,   
        element_ends_row  => iterator_element_ends_row,         
        data_in           => iterator_element_as_BCD, 
        ready_for_input   => UART_ready_for_input
    );
    
  new_data_signal <= '1'  when (iterator_element_available = '1' AND current_state = State_Transmitting) 
                          else '0';
                            
  iterator_step_signal <= '1' when (current_state = State_Calculating and
                                    ((operation = Matrix_Add and adder_complete = '1')
                                    or (operation = Matrix_Multiply and multiplier_complete = '1')))
  
                                    OR
  
                                    (UART_ready_for_input = '1' 
                                    AND current_state = State_Transmitting
                                    AND iterator_element_ends_matrix = '0')  
                                    else '0';
                                                    
  step_clock : entity work.Clock_Divider(Behavioral)
    generic map(
      freq_in  => master_clock_freq,
      freq_out => 500
    )
    port map(
      reset     => '0',
      clock_in  => master_clock,
      clock_out => print_data_signal 
    );

  display_driver : entity work.CCC_Board_Display_Driver(Behavioral)
    port map (
      master_clock          => master_clock,
      message               => display_message,
      decimals              => B"0000",
      board_display_segment => board_display_segment,
      board_display_digit   => board_display_digit,
      board_display_decimal => board_display_decimal
    );  

  debouncer : entity work.Debounce(fsmd_arch)
    port map(
      clk      => master_clock,
      reset    => '0',
      sw       => board_button_center,
      db_level => open, 
      db_tick  => debounced_button_center
    );

  ------------------------------------------------------------------------------ 
  -- Adder / Multiplier Networks
  adder_input_aa_data_out <= matrix_a_port_a_data_out;
  adder_input_ab_data_out <= matrix_a_port_b_data_out;
  adder_input_ba_data_out <= matrix_b_port_a_data_out;
  adder_input_bb_data_out <= matrix_b_port_b_data_out;

  multiplier_input_a_data_out <= matrix_a_port_a_data_out;
  multiplier_input_b_data_out <= matrix_b_port_a_data_out;
  
  iterator_data_in <= matrix_c_port_a_data_out;

  process
  begin
    case current_state is
      when State_Idle         =>
        if board_button_center = '1' and button_is_held = '0' then
          case operation is
            when Matrix_Add      => adder_enable      <= '1';
                                    multiplier_enable <= '0';
            when Matrix_Multiply => multiplier_enable <= '1';
                                         adder_enable <= '0';
          end case;
        end if;
       
        matrix_a_port_a_request <= (No_Op, (to_unsigned(0, row_index_length), 
                                            to_unsigned(0, column_index_length)));
        matrix_a_port_b_request <= (No_Op, (to_unsigned(0, row_index_length), 
                                            to_unsigned(0, column_index_length)));
        matrix_b_port_a_request <= (No_Op, (to_unsigned(0, row_index_length), 
                                            to_unsigned(0, column_index_length)));
        matrix_b_port_b_request <= (No_Op, (to_unsigned(0, row_index_length), 
                                            to_unsigned(0, column_index_length)));

        matrix_c_port_a_request <= (No_Op, (to_unsigned(0, row_index_length), 
                                            to_unsigned(0, column_index_length)));
        matrix_c_port_b_request <= (No_Op, (to_unsigned(0, row_index_length), 
                                            to_unsigned(0, column_index_length)));
        matrix_c_port_a_data_in <= B"0000000000000000";
        matrix_c_port_b_data_in <= B"0000000000000000";

      when State_Calculating  =>
             adder_enable <= '0';
        multiplier_enable <= '0';
      
        case operation is
          when Matrix_Add      => 
            matrix_a_port_a_request <= adder_input_aa_request;
            matrix_a_port_b_request <= adder_input_ab_request;
            matrix_b_port_a_request <= adder_input_ba_request;
            matrix_b_port_b_request <= adder_input_bb_request;
    
            matrix_c_port_a_request <= adder_output_a_request;
            matrix_c_port_b_request <= adder_output_b_request;
            matrix_c_port_a_data_in <= adder_output_a_data_in;
            matrix_c_port_b_data_in <= adder_output_b_data_in;

          when Matrix_Multiply => 
            matrix_a_port_a_request <= multiplier_input_a_request;
            matrix_a_port_b_request <= (No_Op, (to_unsigned(0, row_index_length), 
                                                to_unsigned(0, column_index_length)));
            matrix_b_port_a_request <= multiplier_input_b_request;
            matrix_b_port_b_request <= (No_Op, (to_unsigned(0, row_index_length), 
                                                to_unsigned(0, column_index_length)));
    
            matrix_c_port_a_request <= multiplier_output_request;
            matrix_c_port_b_request <= (No_Op, (to_unsigned(0, row_index_length), 
                                                to_unsigned(0, column_index_length)));
            matrix_c_port_a_data_in <= multiplier_output_data_in;
            matrix_c_port_b_data_in <= B"0000000000000000";
        end case;
        
      when State_Transmitting =>
             adder_enable <= '0';
        multiplier_enable <= '0';
      
        matrix_a_port_a_request <= (No_Op, (to_unsigned(0, row_index_length), 
                                            to_unsigned(0, column_index_length)));
        matrix_a_port_b_request <= (No_Op, (to_unsigned(0, row_index_length), 
                                            to_unsigned(0, column_index_length)));
        matrix_b_port_a_request <= (No_Op, (to_unsigned(0, row_index_length), 
                                            to_unsigned(0, column_index_length)));
        matrix_b_port_b_request <= (No_Op, (to_unsigned(0, row_index_length), 
                                            to_unsigned(0, column_index_length)));

        matrix_c_port_a_request <= iterator_request;
        matrix_c_port_b_request <= (No_Op, (to_unsigned(0, row_index_length), 
                                            to_unsigned(0, column_index_length)));
        matrix_c_port_a_data_in <= B"0000000000000000";
        matrix_c_port_b_data_in <= B"0000000000000000";
    end case;
  end process;

  ------------------------------------------------------------------------------
  -- Controller Logic
  operation <= operation  when (current_state /= State_Idle) else 
               Matrix_Add when (current_state = State_Idle and 
                                board_switch(0) = '1') else
               Matrix_Multiply;  

  ------------------------------------------------------------------------------
  -- Sequential Logic
process (master_clock)
  begin
    if rising_edge(master_clock) then 
      case current_state is
        when State_Idle         => 
          display_message <= to_ccctring("Idle");
          
          if board_button_center = '1' and button_is_held = '0' then
            current_state <= State_Calculating;
            button_is_held <= '1';
          elsif board_button_center = '0' then
            button_is_held <= '0';
          end if;

        when State_Calculating  => 
          display_message <= to_ccctring("Calc");

          if (operation = Matrix_Add      and      adder_complete = '1') or
             (operation = Matrix_Multiply and multiplier_complete = '1') then
            current_state <= State_Transmitting;
          end if;

        when State_Transmitting => 
          display_message <= to_ccctring("Send");
          
          if iterator_element_ends_matrix = '1' and iterator_element_available = '1' then
            current_state <= State_Idle;
          end if;
      end case;
    end if;
  end process;
end Behavioral;
