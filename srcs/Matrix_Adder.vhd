library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.matrices.all;
use work.common.all;

-- So far untested.
-- Start with enable and hold high until the complete signal goes high.
entity Adder is
  generic (
  	matrix_a : Matrix;
  	matrix_b : Matrix;
  	matrix_c : Matrix
  );
  port (
  	clock  : in  Std_Logic;
  	enable : in  Std_Logic;
  	
  	matrix_a_accessor_port_a_request  : out Matrix_Access_Request ( index ( 
                                             row    ((addressing_size(matrix_a.n_rows)    - 1) downto 0),
                                             column ((addressing_size(matrix_a.n_columns) - 1) downto 0)));
  	matrix_a_accessor_port_a_data_out : in Unsigned((as_natural(matrix_a.bitwidth) - 1) downto 0);
  	matrix_a_accessor_port_b_request  : out Matrix_Access_Request ( index ( 
                                             row    ((addressing_size(matrix_a.n_rows)    - 1) downto 0),
                                             column ((addressing_size(matrix_a.n_columns) - 1) downto 0)));
  	matrix_a_accessor_port_b_data_out : in Unsigned((as_natural(matrix_a.bitwidth) - 1) downto 0);
  	
  	matrix_b_accessor_port_a_request  : out Matrix_Access_Request ( index ( 
                                             row    ((addressing_size(matrix_b.n_rows)    - 1) downto 0),
                                             column ((addressing_size(matrix_b.n_columns) - 1) downto 0)));
    matrix_b_accessor_port_a_data_out : in Unsigned((as_natural(matrix_b.bitwidth) - 1) downto 0);
    matrix_b_accessor_port_b_request  : out Matrix_Access_Request ( index ( 
                                             row    ((addressing_size(matrix_b.n_rows)    - 1) downto 0),
                                             column ((addressing_size(matrix_b.n_columns) - 1) downto 0)));
    matrix_b_accessor_port_b_data_out : in Unsigned((as_natural(matrix_b.bitwidth) - 1) downto 0);
  	
  	matrix_c_accessor_port_a_request : out Matrix_Access_Request ( index ( 
                                             row    ((addressing_size(matrix_c.n_rows)    - 1) downto 0),
                                             column ((addressing_size(matrix_c.n_columns) - 1) downto 0)));
  	matrix_c_accessor_port_a_data_in : out Unsigned((as_natural(matrix_c.bitwidth) - 1) downto 0);
  	matrix_c_accessor_port_b_request : out Matrix_Access_Request ( index ( 
                                             row    ((addressing_size(matrix_c.n_rows)    - 1) downto 0),
                                             column ((addressing_size(matrix_c.n_columns) - 1) downto 0)));
    matrix_c_accessor_port_b_data_in : out Unsigned((as_natural(matrix_c.bitwidth) - 1) downto 0);
  	
  	is_complete : out Std_Logic
  );
begin
  Assert are_matrices_addable(matrix_a, matrix_b, matrix_c)
  Report "Matrices provided to Adder are of incompatible size!"
  Severity Failure;
end entity Adder;

architecture Linear of Adder is
  constant input_bitwidth  : Natural := as_natural(matrix_a.bitwidth);
  constant output_bitwidth : Natural := as_natural(matrix_c.bitwidth);
  
  constant input_row_length     : Natural := addressing_size(matrix_a.n_rows);
  constant input_column_length  : Natural := addressing_size(matrix_a.n_columns);
  constant output_row_length    : Natural := addressing_size(matrix_c.n_rows);
  constant output_column_length : Natural := addressing_size(matrix_c.n_columns);

  function b_row_default return Unsigned is
  begin
    if matrix_a.n_columns = 1 then
      return to_unsigned(1, input_row_length);
    else
      return to_unsigned(0, input_row_length);
    end if;
  end function b_row_default;
  
  function b_column_default return Unsigned is
  begin
    if matrix_a.n_columns = 1 then
      return to_unsigned(0, input_row_length);
    else
      return to_unsigned(1, input_column_length);
    end if;
  end function b_column_default;

  signal working     : Std_Logic := '1';
  signal complete    : Std_Logic := '1';
  signal first_cycle : Std_Logic := '1';
  
  type  Input_Array is array (Integer range <>) of Unsigned((2 * input_bitwidth - 1) downto 0);
  type Output_Array is array (Integer range <>) of Unsigned((   output_bitwidth - 1) downto 0);
  
  signal  input :  Input_Array(1 downto 0);
  signal output : Output_Array(1 downto 0);
  
  signal input_operation  : Matrix_Access_Operations(1 downto 0) := (others => No_Op);
  signal input_indices    : Matrix_Indices(1 downto 0) (
      row    (input_row_length    downto 0), 
      column (input_column_length downto 0) 
    ) := (
      0 => ( row    => to_unsigned(0,                (input_row_length    + 1)),
             column => to_unsigned(0,                (input_column_length + 1)) ),
      1 => ( row    =>      resize(b_row_default,    (input_row_length    + 1)),
             column =>      resize(b_column_default, (input_column_length + 1))));
  signal next_input_indices : Matrix_Indices(1 downto 0) (
     row    (input_row_length    downto 0), 
     column (input_column_length downto 0) 
   );       
  
        
  signal output_operation  : Matrix_Access_Operations(1 downto 0) := (others => No_Op);
  signal output_indices    : Matrix_Indices(1 downto 0) (
      row    (output_row_length    downto 0), 
      column (output_column_length downto 0) 
    ) := (
      0 => ( row    => to_unsigned(0,                (output_row_length    + 1)),
             column => to_unsigned(0,                (output_column_length + 1)) ),
      1 => ( row    =>      resize(b_row_default,    (output_row_length    + 1)),
             column =>      resize(b_column_default, (output_column_length + 1)) ));
  
begin

  is_complete <= complete;

  matrix_a_accessor_port_a_request.operator <= input_operation(0);
  matrix_b_accessor_port_a_request.operator <= input_operation(0);
  matrix_a_accessor_port_b_request.operator <= input_operation(1);
  matrix_b_accessor_port_b_request.operator <= input_operation(1);
  
  input(0)(15 downto 8) <= matrix_a_accessor_port_a_data_out;
  input(0)( 7 downto 0) <= matrix_b_accessor_port_a_data_out;
  input(1)(15 downto 8) <= matrix_a_accessor_port_b_data_out;
  input(1)( 7 downto 0) <= matrix_b_accessor_port_b_data_out;
  
  index_iterators : for i in input_indices'range generate
    next_input_indices(i).row    <= 
      (input_indices(i).row + 1)
        when ((input_operation(i) = Element_Read) and 
             ((input_indices(i).column + 2) >= matrix_a.n_columns))
        else 
      input_indices(i).row;
      
    next_input_indices(i).column <= 
      ((input_indices(i).column + 2) - matrix_a.n_columns)
        when ((input_operation(i) = Element_Read) and 
             ((input_indices(i).column + 2) >= matrix_a.n_columns))
        else
      (input_indices(i).column + 2)
        when ((input_operation(i) = Element_Read) and
             ((input_indices(i).column + 2)  < matrix_a.n_columns))
        else
      input_indices(i).column; 
  end generate;
  
  matrix_a_accessor_port_a_request.index <= input_indices(0);
  matrix_b_accessor_port_a_request.index <= input_indices(0);
  matrix_a_accessor_port_b_request.index <= input_indices(1);
  matrix_b_accessor_port_b_request.index <= input_indices(1);

  matrix_c_accessor_port_a_request.operator <= output_operation(0);
  matrix_c_accessor_port_b_request.operator <= output_operation(1);
  matrix_c_accessor_port_a_request.index    <= output_indices(0);
  matrix_c_accessor_port_b_request.index    <= output_indices(1);
  
  output_generation : for i in output'range generate
    output(i) <= resize(input(i)(15 downto 8), output_bitwidth) + 
                 resize(input(i)( 7 downto 0), output_bitwidth);
  end generate;
  
  matrix_c_accessor_port_a_data_in <= output(0) 
                                        when output_operation(0) = Element_Read_Write 
                                        else 
                                      to_unsigned(0, output_bitwidth);
  matrix_c_accessor_port_b_data_in <= output(1) 
                                        when output_operation(1) = Element_Read_Write 
                                        else 
                                      to_unsigned(0, output_bitwidth);
  
  process (clock)
  begin
    if rising_edge(clock) then
      if working = '0' then        
        if enable = '1' then
          working  <= '1';
          complete <= '0';
        else
          working  <= '0';
          complete <= '1';
        end if;
        
      else 
        -- Check if we're all done.
        if (first_cycle = '0') and 
           (input_operation  = (No_Op, No_Op)) and 
           (output_operation = (No_Op, No_Op)) then
          complete <= '1';
        else
          complete <= '0';
        end if;
        
        if complete = '1' then
          working     <= '0';
          first_cycle <= '1';
          
          input_operation  <= (No_Op, No_Op);
          output_operation <= (No_Op, No_Op);
          input_indices    <= (
            0 => ( row    => to_unsigned(0,                (input_row_length    + 1)),
                   column => to_unsigned(0,                (input_column_length + 1)) ),
            1 => ( row    =>      resize(b_row_default,    (input_row_length    + 1)),
                   column =>      resize(b_column_default, (input_column_length + 1))));
          output_indices   <= (
            0 => ( row    => to_unsigned(0,                (output_row_length    + 1)),
                   column => to_unsigned(0,                (output_column_length + 1)) ),
            1 => ( row    =>      resize(b_row_default,    (output_row_length    + 1)),
                   column =>      resize(b_column_default, (output_column_length + 1)) ));
        else
       
          working     <= '1';
          first_cycle <= '0';
           
          -- Commit any of our scheduled reads and iterate the indices.
          for i in input_operation'range loop          
            if is_valid_index(matrix_a, next_input_indices(i)) then
              input_operation(i) <= Element_Read;
            else
              input_operation(i) <= No_Op;
            end if;
            
            input_indices(i) <= next_input_indices(i);
          end loop;
    
          for i in output_operation'range loop
            -- Schedule Writes and Iterate Read Indices
            if (input_operation(i) = Element_Read) then    
              output_operation(i)     <= Element_Read_Write;
              output_indices(i)       <= input_indices(i);
            else
              output_operation(i) <= No_Op;
              output_indices(i)   <= output_indices(i);
            end if;        
          end loop;  
        end if;   
      end if;
    end if;
  end process;
end architecture Linear;