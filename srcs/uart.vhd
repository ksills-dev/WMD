--All "rx" are inputs to FPGA, all "tx" are outputs from FPGA to terminal 


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity uart is
   generic(
     -- Default setting:
     -- 19,200 baud, 8 data bis, 1 stop its, 2^2 FIFO
      
      DBIT:     integer := 8;   -- # data bits
      SB_TICK:  integer := 16;  -- # ticks for stop bits, 16/24/32 for 1/1.5/2 stop bits
      DVSR:     integer := 326; -- baud rate divisor, DVSR = 100M/(16*baud rate)
      DVSR_BIT: integer := 9;   -- # bits of DVSR
      FIFO_W:   integer := 4    -- # addr bits of FIFO, words in FIFO=2^FIFO_W
 
   );
   port(
      clock, reset:       in std_logic;
      data_in:            in std_logic_vector(19 downto 0);
      new_data_signal:    in std_logic;
      element_ends_row:   in std_logic;
      rd_uart, wr_uart:   in std_logic;
      w_data:             in std_logic_vector(7 downto 0);
      tx_full, rx_empty:  out std_logic;
      r_data:             out std_logic_vector(7 downto 0);
      tx:                 out std_logic;
      ready_for_input :    out STD_LOGIC
   );
   
end uart;

architecture str_arch of uart is
   signal tick: std_logic;
   signal rx_done_tick: std_logic;
   signal tx_fifo_out: std_logic_vector(7 downto 0);
   signal rx_data_out: std_logic_vector(7 downto 0);
   signal tx_empty, tx_fifo_not_empty: std_logic;
   signal tx_done_tick: std_logic;
   signal rx_full: std_logic;
    
    component   binary_to_ascii_tx
        Port (
             clock :              in STD_LOGIC;
             binary_in :          in UNSIGNED (19 downto 0);
             new_data_signal:     in STD_LOGIC;
             element_ends_row:    in STD_LOGIC;
             rx_full :            in STD_LOGIC;
             ascii_out :          out UNSIGNED (7 downto 0);  
             tx_done_tick :       out STD_LOGIC;
             ready_for_input :    out STD_LOGIC
         );
      end component;
  
  
  signal ascii_out_wire : std_logic_vector(7 downto 0);
  
begin
    baud_gen_unit: entity work.mod_m_counter(arch)
        generic map (M=>DVSR, N=>DVSR_BIT)
        port map (clk=>clock, reset=>reset,
                  q=>open, max_tick=>tick);

--For serial input, not in use right now, replaced by our binary to ascii 

--    uart_rx_unit: entity work.uart_rx(arch)
--        generic map (DBIT=>DBIT, SB_TICK=>SB_TICK)
--        port map (
--          clk           => clk,           --input 
--          reset         => reset,         --input   
--          rx            => rx,            --input
--          s_tick        => tick,          --input 
--          rx_done_tick  => rx_done_tick,  --output        
--          dout          => rx_data_out    --output        
--        );
                  
    binary_input : binary_to_ascii_tx
        port map(
          clock                         => clock,
          binary_in                     => unsigned(data_in), 
          new_data_signal               => new_data_signal,
          element_ends_row              => element_ends_row,
          rx_full                       => rx_full,
          std_logic_vector (ascii_out)  => rx_data_out,
          tx_done_tick                  => rx_done_tick,
          ready_for_input               => ready_for_input
        );    

    fifo_rx_unit: entity work.fifo(arch)
        generic map (B=>DBIT, W=>FIFO_W)
        port map (
            clk     => clock,           --input clock
            reset   => reset,           --input reset
            rd      => rd_uart,         --input 
            wr      => rx_done_tick,    --input
            w_data  => rx_data_out,     --input
            empty   => rx_empty,        --output
            full    => rx_full,         --output
            r_data  => r_data           --output
          );        
          
    fifo_tx_unit: entity work.fifo(arch)
        generic map (B=>DBIT, W=>FIFO_W)
        port map (
            clk     => clock,            --input
            reset   => reset,          --in
            rd      => tx_done_tick,   --in
            wr      => wr_uart,        --in
            w_data  => rx_data_out,    --in
            empty   => tx_empty,       --out
            full    => tx_full,        --out
            r_data  => tx_fifo_out     --out
        );


                               
    uart_tx_unit: entity work.uart_tx(arch)
        generic map (DBIT=>DBIT, SB_TICK=>SB_TICK)
        port map (
          clk           => clock,             --input
          reset         => reset,             --input
          tx_start      => tx_fifo_not_empty, --input
          s_tick        => tick,              --input 
          din           => tx_fifo_out,       --input
          tx_done_tick  => tx_done_tick,      --output
          tx            => tx                 --output
        );
    
    tx_fifo_not_empty <= not tx_empty;

end str_arch;
