library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.common.all;
use work.matrices.all;
use work.hw_info.all;
use work.ccc.all;

entity Accessor_Test_Circuit is
  port (
    master_clock          : in  Std_Logic;
    
    board_button_center   : in  Std_Logic;
    board_button_left     : in  Std_Logic;
    board_button_right    : in  Std_Logic;
    
    board_switch          : in  Std_Logic_Vector(15 downto 0);
    
    board_led             : out Std_Logic_Vector(15 downto 0);
    board_display_segment : out Std_Logic_Vector(6 downto 0);
    board_display_digit   : out Std_Logic_Vector(3 downto 0);
    board_display_decimal : out Std_Logic

  );
end entity Accessor_Test_Circuit;

architecture Behavioral of Accessor_Test_Circuit is
  signal a_request : Matrix_Access_Request ( 
      index ( row (1 downto 0), column (1 downto 0) ) 
    ) := (
    operator => Element_Read, 
    index    => (
      row    => to_unsigned(0, 2), 
      column => to_unsigned(0, 2)
    )
  );
    
  signal b_request : Matrix_Access_Request ( 
      index ( row (1 downto 0), column (1 downto 0) ) 
    ) := (
    operator => Element_Read, 
    index    => (
      row    => to_unsigned(0, 2), 
      column => to_unsigned(0, 2)
    )
  );
  
  signal a_data_out : Unsigned(3 downto 0);
  signal b_data_out : Unsigned(3 downto 0);
begin

  a_request.index.row    <= Unsigned(board_switch(15 downto 14));
  a_request.index.column <= Unsigned(board_switch(13 downto 12));
  b_request.index.row    <= Unsigned(board_switch(7 downto 6));
  b_request.index.column <= Unsigned(board_switch(5 downto 4));

  display : entity work.CCC_Board_Display_Driver(Behavioral)
    port map(
      master_clock          => master_clock,
      message(3)            => to_ccchar(' '),
      message(2)            => Std_Logic_Vector(a_data_out),
      message(1)            => Std_Logic_Vector(b_data_out),
      message(0)            => to_ccchar(' '),
      decimals              => B"0100",
      board_display_segment => board_display_segment,
      board_display_digit   => board_display_digit,
      board_display_decimal => board_display_decimal
    );

  matrix : entity work.Matrix_Accessor(Dataflow)
    generic map (
      description => ( 
        n_rows    => 4, 
        n_columns => 4, 
        bitwidth  => Width_8 ),
      block_count => 25
    )
    port map (
      clock => master_clock,
      
      port_a_request              => a_request,
      port_a_data_in (7 downto 4) => B"0000",
      port_a_data_in (3 downto 0) => Unsigned(board_switch(11 downto 8)),
      port_a_data_out(7 downto 4) => open,
      port_a_data_out(3 downto 0) => a_data_out,
      
      port_b_request              => b_request,
      port_b_data_in (7 downto 4) => B"0000",
      port_b_data_in (3 downto 0) => Unsigned(board_switch(3 downto 0)),
      port_b_data_out(7 downto 4) => open,
      port_b_data_out(3 downto 0) => b_data_out
    );

  process (master_clock)
  begin
    if rising_edge(master_clock) then
      if board_button_center = '1' then
        a_request.operator <= Element_Read_Write;
        b_request.operator <= Element_Read_Write;
      elsif board_button_left = '1' then
        a_request.operator <= Element_Read_Write;
        b_request.operator <= Element_Read;
      elsif board_button_right = '1' then
        a_request.operator <= Element_Read;
        b_request.operator <= Element_Read_Write;
      else 
        a_request.operator <= Element_Read;
        b_request.operator <= Element_Read;
      end if;
    end if;
  end process;
end Behavioral;
