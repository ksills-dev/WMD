--PuTTY, select "Serial", baud rate is 19200. 
--Go Device Manager -> Ports to find which COM the FPGA is on
--Each tick is currently on up button btnU

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity uart_top is
   port(
       clock             : in  Std_Logic; 
       reset             : in  Std_Logic;
       
       print_data_signal : in  Std_Logic;
       new_data_signal   : in  Std_Logic;
       element_ends_row  : in  Std_Logic;
       data_in           : in  Std_Logic_Vector(19 downto 0);
       
       tx                : out Std_Logic;
       ready_for_input   : out Std_Logic
);
end entity uart_top;

architecture arch of uart_top is
   signal tx_full  : Std_Logic; 
   signal rx_empty : Std_Logic;
   signal rec_data : Std_Logic_Vector(7 downto 0);
   signal btn_tick : Std_Logic;
   signal count    : Unsigned(15 downto 0);
   
   component rams_init_file
       port(
           clock: 	in std_logic;
           address: in unsigned(15 downto 0);
           dout: 	out unsigned(7 downto 0)
       );
   end component;
   
begin
  -- instantiate uart
  uart_unit: entity work.uart(str_arch)
    port map(
      clock 	         => clock, 
      reset 	         => reset, 

      new_data_signal  => new_data_signal, 
      element_ends_row => element_ends_row,
      data_in          => data_in,
      rd_uart          => print_data_signal,--btn_tick,
      wr_uart          => print_data_signal,--btn_tick, 
      w_data 	         => rec_data,
      tx_full          => tx_full, 
      rx_empty         => rx_empty,
      r_data 	         => rec_data,
      tx               => tx,
      ready_for_input  => ready_for_input
      );                  
      
--    btn_db_unit: entity work.debounce(fsmd_arch)
--     port map(
--         clk=>clock, 
--         reset=>reset, 
--         sw=>print_data_signal,
--         db_level=>open, 
--         db_tick=>btn_tick
--   );
end architecture arch;
