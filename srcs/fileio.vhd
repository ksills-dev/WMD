-- Initializing Block RAM from external data file
-- File: rams_init_file.vhd
-- Page 127 of https://www.xilinx.com/support/documentation/sw_manuals/xilinx2015_2/ug901-vivado-synthesis.pdf

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use std.textio.all;
use IEEE.NUMERIC_STD.ALL;

entity rams_init_file is
    port(
        clock : in std_logic;
        address : in unsigned(15 downto 0);
        dout : out unsigned(7 downto 0)
    );
    
end rams_init_file;

architecture syn of rams_init_file is

    type RamType is array (0 to 64) of bit_vector(7 downto 0);
    
    impure function InitRamFromFile(RamFileName : in string) return RamType is
        FILE RamFile : text is in RamFileName;
        variable RamFileLine : line;
        variable RAM : RamType;
        begin
            for I in RamType'range loop
                readline(RamFile, RamFileLine);
                read(RamFileLine, RAM(I));
            end loop;
        return RAM;
    end function;
    
    signal RAM : RamType := InitRamFromFile("rams_init_file.data");
    
    begin
        process(clock)
        begin    
            dout <= unsigned(to_stdlogicvector(RAM(to_integer(address))));
        end process;
    end syn;