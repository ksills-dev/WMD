library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.matrices.all;
use work.common.all;

entity Matrix_Multiplier is
  generic (
    matrix_a : Matrix;
    matrix_b : Matrix;
    matrix_c : Matrix
  );
  port (
    clock  : in  Std_Logic;
  	enable : in  Std_Logic;
  	
  	matrix_a_request  : out Matrix_Access_Request ( index ( 
                              row    ((addressing_size(matrix_a.n_rows)    - 1) downto 0),
                              column ((addressing_size(matrix_a.n_columns) - 1) downto 0)));
  	matrix_a_data_out : in Unsigned((as_natural(matrix_a.bitwidth) - 1) downto 0);
  	matrix_b_request  : out Matrix_Access_Request ( index ( 
                              row    ((addressing_size(matrix_b.n_rows)    - 1) downto 0),
                              column ((addressing_size(matrix_b.n_columns) - 1) downto 0)));
    matrix_b_data_out : in Unsigned((as_natural(matrix_a.bitwidth) - 1) downto 0);

    matrix_c_request  : out Matrix_Access_Request ( index ( 
                              row    ((addressing_size(matrix_c.n_rows)    - 1) downto 0),
                              column ((addressing_size(matrix_c.n_columns) - 1) downto 0)));
    matrix_c_data_in  : out Unsigned((as_natural(matrix_c.bitwidth) - 1) downto 0);
    
    is_complete : out Std_Logic
  );
begin
  Assert are_matrices_multipliable(matrix_a, matrix_b, matrix_c)
  Report "Matrices provided to Multiplier are of incompatible size!"
  Severity Failure;
end entity Matrix_Multiplier;

-- For each row in A
  --   For each column in B
  --     For each n
architecture Linear of Matrix_Multiplier is
  constant input_a_row_length    : Natural := addressing_size(matrix_a.n_rows);
  constant input_b_column_length : Natural := addressing_size(matrix_b.n_columns);
  constant input_shared_length   : Natural := addressing_size(matrix_a.n_columns);

  constant  input_bitwidth : Natural := as_natural(matrix_a.bitwidth);
  constant output_bitwidth : Natural := as_natural(matrix_c.bitwidth);

  signal  input_operation : Matrix_Access_Operation := No_Op;
  signal output_operation : Matrix_Access_Operation := No_Op;

  signal input_a_row    : Unsigned(input_a_row_length    downto 0) := to_unsigned(0, input_a_row_length    + 1);
  signal input_b_column : Unsigned(input_b_column_length downto 0) := to_unsigned(0, input_b_column_length + 1);
  signal input_shared   : Unsigned(input_shared_length   downto 0) := to_unsigned(0, input_shared_length   + 1);

  signal next_input_a_row    : Unsigned(input_a_row_length    downto 0) := to_unsigned(0, input_a_row_length    + 1);
  signal next_input_b_column : Unsigned(input_b_column_length downto 0) := to_unsigned(0, input_b_column_length + 1);
  signal next_input_shared   : Unsigned(input_shared_length   downto 0) := to_unsigned(0, input_shared_length   + 1);

  signal output_row    : Unsigned(input_a_row_length    downto 0) := to_unsigned(0, input_a_row_length    + 1);
  signal output_column : Unsigned(input_b_column_length downto 0) := to_unsigned(0, input_b_column_length + 1);

  signal product          : Unsigned(output_bitwidth - 1 downto 0) := to_unsigned(0, output_bitwidth);
  signal accumulator      : Unsigned(output_bitwidth - 1 downto 0) := to_unsigned(0, output_bitwidth);
  signal next_accumulator : Unsigned(output_bitwidth - 1 downto 0) := to_unsigned(0, output_bitwidth);

  signal complete : Std_Logic := '1';
  signal working  : Std_Logic := '0';
begin

  is_complete <= complete;
  
  matrix_a_request.operator     <= input_operation;
  matrix_a_request.index.row    <= input_a_row (input_a_row_length  - 1 downto 0);
  matrix_a_request.index.column <= input_shared(input_shared_length - 1 downto 0);
  
  matrix_b_request.operator     <= input_operation;
  matrix_b_request.index.row    <= input_shared  (input_shared_length   - 1 downto 0);
  matrix_b_request.index.column <= input_b_column(input_b_column_length - 1 downto 0); 
  
  matrix_c_request.operator     <= output_operation;
  matrix_c_request.index.row    <= output_row   (input_a_row_length    - 1 downto 0);
  matrix_c_request.index.column <= output_column(input_b_column_length - 1 downto 0);

  input_operation <= Element_Read
                       when (working = '1') and (input_a_row < matrix_a.n_rows)
                       else
                      No_Op;

  output_operation <= Element_Read_Write
                        when (working = '1') and 
                             (input_shared = 0) and 
                             ((input_a_row /= 0) or (input_b_column /= 0))
                        else
                      No_Op;

  product          <= matrix_a_data_out * matrix_b_data_out;
  next_accumulator <= accumulator + product;
  matrix_c_data_in <= next_accumulator;

  next_input_a_row    <= input_a_row + 1
                          when (next_input_b_column = matrix_b.n_columns)
                          else
                         input_a_row;
  next_input_b_column <= input_b_column + 1
                           when (next_input_shared = matrix_a.n_columns)
                           else
                         input_b_column;
  next_input_shared   <= input_shared + 1;

  working <= '1' when (complete = '0') or 
                      ((complete = '1') and (enable = '1')) else 
             '0';

  process (clock)
  begin
    if rising_edge(clock) then
      complete <= '1' when (working = '0') or
                           ((input_a_row = matrix_a.n_rows) and
                            (output_operation = Element_Read_Write)) else
                  '0';
    
      if working = '1' then
        if (input_shared = 0) then
          accumulator <= to_unsigned(0, output_bitwidth);
        else
          accumulator <= next_accumulator;
        end if;

        if (input_operation = Element_Read) then
          output_row    <= input_a_row;
          output_column <= input_b_column;

          input_a_row   <= next_input_a_row;

          if (next_input_b_column = matrix_b.n_columns) then
            input_b_column <= to_unsigned(0, input_b_column_length + 1);
          else
            input_b_column <= next_input_b_column;
          end if;

          if (next_input_shared = matrix_a.n_columns) then
            input_shared <= to_unsigned(0, input_shared_length + 1);
          else
            input_shared <= next_input_shared;
          end if;
        end if;
      else
          accumulator    <= to_unsigned(0, output_bitwidth);
          input_a_row    <= to_unsigned(0, input_a_row_length + 1);
          input_b_column <= to_unsigned(0, input_b_column_length + 1);
          input_shared   <= to_unsigned(0, input_shared_length + 1);
      end if;
    end if;
  end process;
end architecture Linear;