----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/14/2018 10:54:43 AM
-- Design Name: 
-- Module Name: binary_to_ascii_tx - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bcd_to_ascii is
    generic (
        N : integer := 20
    );
    
    Port ( 
           clock :              in STD_LOGIC;
           tick :               in STD_LOGIC;
           button_tick:         in STD_LOGIC;
           rx_full:             in STD_LOGIC;
           new_data_signal :    in STD_LOGIC;
           element_ends_row :   in STD_LOGIC;
           bcd_in :             in UNSIGNED (N-1 downto 0);  --Largest is 2^16 -1 = 65535, 5 digits each 4 bits
           ascii_out :          out UNSIGNED (7 downto 0);  
           tx_done_tick :       out STD_LOGIC;--;
           ready_for_input :    out STD_LOGIC
    );
    
end bcd_to_ascii;

architecture Behavioral of bcd_to_ascii is

type state_type IS (start_state, is_full_state, 
leading_zeros_state,
--zero_input_state, 
calculate_state, transmit_state);

signal state : state_type := start_state;
signal zero_flag :      std_logic := '0';
signal current_data :   UNSIGNED (N-1 downto 0) := (others => '0');
signal M :              integer := 0;

signal ready : Std_Logic := '1';

begin
    ready_for_input <= ready;

    process(clock)
    begin
    
        if rising_edge(clock) then
            
            case state is
                
                --Waits for new_data_signal to indicate a new number. Resets counter "M", loads new BCD data into current_data
                when start_state =>
                    
                    if (new_data_signal = '1') then
                        ready <= '0';
                        state <= leading_zeros_state; 
                        M <= 0;
                        current_data <= bcd_in;
                    else
                        M <= 0;
                    end if;
                
                --Checks if the FIFO is full, waits until it is to proceed.    
                when is_full_state =>
                
                    if rx_full = '0' then
                        state <= calculate_state;
                    end if;      
                
                --Strips leading zeroes from 5 digit BCD. current_data is read 4 bits at a time (1 digit BCD = 4 bits binary)
                --If the first digit = 0, increment M by 4 and repeat (effectively "skipping" that round of printing)
                --Continue until a non-zero digit is encountered.
                --If the data on the line is a zero, trigger the zero flag and move to the calculate state. 
                when leading_zeros_state =>
                
                    if current_data = 0 then
                        zero_flag <= '1';
                        state <= calculate_state;
                    
                    elsif current_data (N-M-1 downto N-M-4) = 0 then
                        M <= M + 4;
                        state <= leading_zeros_state;
                    else
                        state <= calculate_state;
                    end if;
                
                --If there's a zero on the line, output a single zero, move counter to very end, and output a tab/new line 
                --If counter hasn't reached the end, output the current digit, increment counter, repeat.
                --If counter has reached the end, check if it's the end of the row. if so, print new line. Otherwise, print a tab.
                --If a tab or new line has been printed, trigger the tri-state buffer "ZZZZZZZZ" to be held on the line. 
                when calculate_state =>
                if rx_full = '0' then
                    
                    if zero_flag = '1' then
                        zero_flag <= '0';
                        ascii_out <= "00000000" + 48;
                        M <= 20;
                        state <= transmit_state;
                    
                    elsif M < 20 then
                        if current_data (N-M-1 downto N-M-4) < 10 then
                            ascii_out <= ("0000" & current_data (N-M-1 downto N-M-4)) + 48; -- 0 through 9
                        else
                            ascii_out <= ("0000" & current_data (N-M-1 downto N-M-4)) + 87; -- a through f
                        end if;
                        
                        M <= M + 4;
                        state <= transmit_state;
                        
                    elsif M = 20 then
                        if element_ends_row = '0' then
                            ascii_out <= "00001001"; --tab
                        else
                            ascii_out <= "00001100"; --new line
                        end if;
                        
                        M <= M + 4;
                        state <= transmit_state;
                    
                    else
                        ascii_out <= "ZZZZZZZZ";
--                        ascii_out <= "01000001";
                        M <= M + 4;
                        state <= transmit_state;
                    end if;
                end if;

                --If the tristate buffer's been activated, trigger the ready_for_input flag, and go to the start to wait for input.
                --Otherwise, transmit the data on tx_done_tick, and repeat.
                when transmit_state =>
                    if M = 28 then
                        state <= start_state;
                        ready <= '1';
                    else
                        state <= is_full_state;
                    end if;
                                    
            end case;
            
            tx_done_tick <= '1' when (state = transmit_state) else
                            '0';
                        
              
        end if;
    end process;
end Behavioral;
