library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

-- I really should just use the Clock Wizard provided in the Xilinx
-- IP catalog, but I feel it's more suited to the spirit of this
-- assignment to implement my own counter-based Clock Divider solution.
entity Clock_Divider is
  Generic ( freq_in   : Integer;
            freq_out  : Integer );
              
  Port    ( reset     : in  Std_Logic;
            clock_in  : in  Std_Logic;
            clock_out : out Std_Logic);
end Clock_Divider;

architecture Behavioral of Clock_Divider is
  constant bitwidth  : Natural := Natural(ceil(log2(Real(freq_in)))) + 1;
  constant in_gt_out : Boolean := freq_in >= freq_out;
  constant in_eq_out : Boolean := freq_in = freq_out;
    
  signal counter : Unsigned(bitwidth - 1 downto 0) := to_unsigned(0, bitwidth);
begin
  ASSERT in_eq_out
  REPORT "Input and Output clocks identical (" & 
          Integer'Image(freq_in) &
         " Hz). Remove redundant Clock_Divider."
  SEVERITY Warning;
    
  ASSERT in_gt_out
  REPORT "Clock_Divider cannot multiply clock signals - " &
         "Input Frequency (Hz): " & Integer'Image(freq_in) & "; " &
         "Output Frequency (Hz): " & Integer'Image(freq_out)
  SEVERITY Failure;
    
  process (clock_in, reset)
  begin
    if reset = '1' then
      counter <= to_unsigned(0, bitwidth);    
    elsif rising_edge(clock_in) then
      if counter = to_unsigned(freq_in / freq_out - 1, bitwidth) then
        clock_out <= '1';
        counter <= to_unsigned(0, bitwidth);
      else
        clock_out <= '0';
        counter <= counter + 1;
      end if;
    end if;
  end process;
end Behavioral;
