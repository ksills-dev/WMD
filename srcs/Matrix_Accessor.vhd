library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use IEEE.std_logic_textio.all;

use std.textio.all;

library unisim;
use unisim.vcomponents.all;

use work.common.all;
use work.hw_info.all;
use work.matrices.all;

-- Generally tested (not comprehensively).
entity Matrix_Accessor is
  generic (
    description : Matrix;
    block_count : Natural;
    read_only   : Boolean := False;
    init_file   : String := ""
  );

  port (
    clock           : in Std_Logic;

    port_a_request  : in  Matrix_Access_Request ( index ( 
                            row    ((addressing_size(description.n_rows)    - 1) downto 0),
                            column ((addressing_size(description.n_columns) - 1) downto 0)));
    port_a_data_in  : in  Unsigned((as_natural(description.bitwidth) - 1) downto 0);
    port_a_data_out : out Unsigned((as_natural(description.bitwidth) - 1) downto 0);

    port_b_request  : in  Matrix_Access_Request ( index (
                            row    ((addressing_size(description.n_rows)    - 1) downto 0),
                            column ((addressing_size(description.n_columns) - 1) downto 0)));
    port_b_data_in  : in  Unsigned((as_natural(description.bitwidth) - 1) downto 0);
    port_b_data_out : out Unsigned((as_natural(description.bitwidth) - 1) downto 0)
  );
begin
  Assert ((block_count * block_ram_segment_size) / as_natural(description.bitwidth)) >=
          (description.n_rows * description.n_columns)
  Report "Insufficient block RAM allocated for vector's given size."
  Severity Failure;
end entity Matrix_Accessor;

-- Each of the access ports have unique ownership of their respective RAM block
-- ports. This allows them to operate completely independently.
architecture Dataflow of Matrix_Accessor is
  type Value_Array is
    array (((block_count * block_ram_segment_size / as_natural(description.bitwidth)) - 1) downto 0) of 
    Unsigned((as_natural(description.bitwidth) - 1) downto 0);

  impure function generate_init_data(file_name : in String)  return Value_Array is
    file     data_in      : Text;
    variable file_status  : File_Open_Status;
    variable result       : Value_Array := (others => to_unsigned(0, as_natural(description.bitwidth)));
    variable value        : Bit_Vector(7 downto 0);
    variable counter      : Integer := 0;
    variable current_line : Line;
  begin
    if file_name /= "" then
      file_open(file_status, data_in, file_name, Read_Mode);      
      Assert file_status = Open_Ok
      Report "Provided RAM initialization file could not be opened."
      Severity Failure;

      while not ENDFILE(data_in) and counter <= result'high loop
        readline(data_in, current_line);
        hread(current_line, value);
        result(counter) := resize(Unsigned(to_StdLogicVector(value)), result(counter)'high + 1);
        counter := counter + 1;
      end loop;

      file_close(data_in);
    end if;

    return result;
  end function generate_init_data;

  subtype RAM_Line is Std_Logic_Vector(255 downto 0);
  type RAM_Line_Vector is
    array (((block_count * 64) - 1) downto 0) of
    RAM_Line;
  function split_to_ram_lines(init_data : in Value_Array) return RAM_Line_Vector is
    variable element_index  : Natural := 0;
    variable result         : RAM_Line_Vector;
    
    file data_out : Text;
    variable current_line : Line;
  begin    
    for i in result'low to result'high loop
      for j in 0 to ((256 / as_natural(description.bitwidth)) - 1) loop
        result(i)((((j + 1) * as_natural(description.bitwidth)) - 1) downto
                 (j * as_natural(description.bitwidth))) := Std_Logic_Vector(init_data(element_index));
        element_index := element_index + 1;
      end loop;
    end loop;    
    return result;
  end function split_to_ram_lines;

  constant static_value_array : Value_Array := generate_init_data(init_file);
  constant static_init_data : RAM_Line_Vector := split_to_ram_lines(static_value_array);

  constant bitwidth        : Natural := as_natural(description.bitwidth);
  constant addressing_base : Natural := 3 + (bitwidth / 16);

  type Block_RAM_Interface is record
    port_a_read_enable  : Std_Logic;
    port_b_read_enable  : Std_Logic;
    port_a_write_enable : Std_Logic;
    port_b_write_enable : Std_Logic;

    port_a_data_out     : Std_Logic_Vector(15 downto 0);    
    port_b_data_out     : Std_Logic_Vector(15 downto 0);
  end record Block_RAM_Interface;

  type Block_Interface_Array is array ((block_count - 1) downto 0) of Block_RAM_Interface;
  signal block_interfaces : Block_Interface_Array;
  
  type Shared_RAM_Interface is record
    port_a_addr         : Std_Logic_Vector(13 downto 0);
    port_b_addr         : Std_Logic_Vector(13 downto 0);
    port_a_data_in      : Std_Logic_Vector(15 downto 0);
    port_b_data_in      : Std_Logic_Vector(15 downto 0);
  end record Shared_RAM_Interface;
  signal shared_interface : Shared_Ram_Interface := (
    B"11111111111111"  , B"11111111111111",
    B"0000000000000000", B"0000000000000000"  
  );

  constant flat_index_length : Natural := addressing_size(description.n_rows * description.n_columns);
  signal port_a_flat_index   : Unsigned((flat_index_length - 1) downto 0);
  signal port_b_flat_index   : Unsigned((flat_index_length - 1) downto 0);

  signal port_a_block        : Unsigned((addressing_size(block_count) - 1) downto 0);
  signal port_a_block_index  : Unsigned((13 - addressing_base) downto 0);
  signal port_b_block        : Unsigned((addressing_size(block_count) - 1) downto 0);
  signal port_b_block_index  : Unsigned((13 - addressing_base) downto 0);
begin
  ram_blocks : for i in 0 to (block_count - 1) generate
    ram_block : RAMB18E1
      generic map (
        read_width_a  => bitwidth + (bitwidth / 8),
        write_width_a => bitwidth + (bitwidth / 8),
        read_width_b  => bitwidth + (bitwidth / 8),
        write_width_b => bitwidth + (bitwidth / 8),

        -- Map initializer values.
        INIT_00 => to_bitvector(static_init_data(i * 64 + 0)),
        INIT_01 => to_bitvector(static_init_data(i * 64 + 1)),
        INIT_02 => to_bitvector(static_init_data(i * 64 + 2)),
        INIT_03 => to_bitvector(static_init_data(i * 64 + 3)),
        INIT_04 => to_bitvector(static_init_data(i * 64 + 4)),
        INIT_05 => to_bitvector(static_init_data(i * 64 + 5)),
        INIT_06 => to_bitvector(static_init_data(i * 64 + 6)),
        INIT_07 => to_bitvector(static_init_data(i * 64 + 7)),
        INIT_08 => to_bitvector(static_init_data(i * 64 + 8)),
        INIT_09 => to_bitvector(static_init_data(i * 64 + 9)),
        INIT_0A => to_bitvector(static_init_data(i * 64 + 10)),
        INIT_0B => to_bitvector(static_init_data(i * 64 + 11)),
        INIT_0C => to_bitvector(static_init_data(i * 64 + 12)),
        INIT_0D => to_bitvector(static_init_data(i * 64 + 13)),
        INIT_0E => to_bitvector(static_init_data(i * 64 + 14)),
        INIT_0F => to_bitvector(static_init_data(i * 64 + 15)),
        INIT_10 => to_bitvector(static_init_data(i * 64 + 16)),
        INIT_11 => to_bitvector(static_init_data(i * 64 + 17)),
        INIT_12 => to_bitvector(static_init_data(i * 64 + 18)),
        INIT_13 => to_bitvector(static_init_data(i * 64 + 19)),
        INIT_14 => to_bitvector(static_init_data(i * 64 + 20)),
        INIT_15 => to_bitvector(static_init_data(i * 64 + 21)),
        INIT_16 => to_bitvector(static_init_data(i * 64 + 22)),
        INIT_17 => to_bitvector(static_init_data(i * 64 + 23)),
        INIT_18 => to_bitvector(static_init_data(i * 64 + 24)),
        INIT_19 => to_bitvector(static_init_data(i * 64 + 25)),
        INIT_1A => to_bitvector(static_init_data(i * 64 + 26)),
        INIT_1B => to_bitvector(static_init_data(i * 64 + 27)),
        INIT_1C => to_bitvector(static_init_data(i * 64 + 28)),
        INIT_1D => to_bitvector(static_init_data(i * 64 + 29)),
        INIT_1E => to_bitvector(static_init_data(i * 64 + 30)),
        INIT_1F => to_bitvector(static_init_data(i * 64 + 31)),
        INIT_20 => to_bitvector(static_init_data(i * 64 + 32)),
        INIT_21 => to_bitvector(static_init_data(i * 64 + 33)),
        INIT_22 => to_bitvector(static_init_data(i * 64 + 34)),
        INIT_23 => to_bitvector(static_init_data(i * 64 + 35)),
        INIT_24 => to_bitvector(static_init_data(i * 64 + 36)),
        INIT_25 => to_bitvector(static_init_data(i * 64 + 37)),
        INIT_26 => to_bitvector(static_init_data(i * 64 + 38)),
        INIT_27 => to_bitvector(static_init_data(i * 64 + 39)),
        INIT_28 => to_bitvector(static_init_data(i * 64 + 40)),
        INIT_29 => to_bitvector(static_init_data(i * 64 + 41)),
        INIT_2A => to_bitvector(static_init_data(i * 64 + 42)),
        INIT_2B => to_bitvector(static_init_data(i * 64 + 43)),
        INIT_2C => to_bitvector(static_init_data(i * 64 + 44)),
        INIT_2D => to_bitvector(static_init_data(i * 64 + 45)),
        INIT_2E => to_bitvector(static_init_data(i * 64 + 46)),
        INIT_2F => to_bitvector(static_init_data(i * 64 + 47)),
        INIT_30 => to_bitvector(static_init_data(i * 64 + 48)),
        INIT_31 => to_bitvector(static_init_data(i * 64 + 49)),
        INIT_32 => to_bitvector(static_init_data(i * 64 + 50)),
        INIT_33 => to_bitvector(static_init_data(i * 64 + 51)),
        INIT_34 => to_bitvector(static_init_data(i * 64 + 52)),
        INIT_35 => to_bitvector(static_init_data(i * 64 + 53)),
        INIT_36 => to_bitvector(static_init_data(i * 64 + 54)),
        INIT_37 => to_bitvector(static_init_data(i * 64 + 55)),
        INIT_38 => to_bitvector(static_init_data(i * 64 + 56)),
        INIT_39 => to_bitvector(static_init_data(i * 64 + 57)),
        INIT_3A => to_bitvector(static_init_data(i * 64 + 58)),
        INIT_3B => to_bitvector(static_init_data(i * 64 + 59)),
        INIT_3C => to_bitvector(static_init_data(i * 64 + 60)),
        INIT_3D => to_bitvector(static_init_data(i * 64 + 61)),
        INIT_3E => to_bitvector(static_init_data(i * 64 + 62)),
        INIT_3F => to_bitvector(static_init_data(i * 64 + 63))
      )
      port map (
        -- Generic
        ClkARdClk => clock,
        ClkBWrClk => clock,

        -- Port A
        EnARdEn           => block_interfaces(i).port_a_read_enable,
        WEA(1)            => block_interfaces(i).port_a_write_enable,
        WEA(0)            => block_interfaces(i).port_a_write_enable,
        AddrARdAddr       => shared_interface.port_a_addr,
        DIADI             => shared_interface.port_a_data_in,
        DOADO             => block_interfaces(i).port_a_data_out,

        -- Port B
        EnBWrEn           => block_interfaces(i).port_b_read_enable,
        WEBWE(3 downto 2) => (others => '0'),
        WEBWE(1)          => block_interfaces(i).port_b_write_enable,
        WEBWE(0)          => block_interfaces(i).port_b_write_enable,
        AddrBWrAddr       => shared_interface.port_b_addr,
        DIBDI             => shared_interface.port_b_data_in,
        DOBDO             => block_interfaces(i).port_b_data_out,

        -- Don't Care
        DIPADIP => (others => '0'),
        DIPBDIP => (others => '0'),
        
        RegCEARegCE   => '0',
        RegCEB        => '0',
        RstRamARstRam => '0',
        RstRamB       => '0',
        RstRegARstReg => '0',
        RstRegB       => '0'
      );
      
      block_interfaces(i).port_a_read_enable  <= '1' when ((to_integer(port_a_block) = i) and 
                                                          ((port_a_request.operator = Element_Read) or
                                                           (port_a_request.operator = Element_Read_Write))) else 
                                                 '0';
      block_interfaces(i).port_a_write_enable <= '1' when ((to_integer(port_a_block) = i) and
                                                           ((port_a_request.operator = Element_Write) or
                                                            (port_a_request.operator = Element_Read_Write))) else 
                                                 '0';
      block_interfaces(i).port_b_read_enable  <= '1' when ((to_integer(port_b_block) = i) and 
                                                            ((port_b_request.operator = Element_Read) or
                                                             (port_b_request.operator = Element_Read_Write))) else 
                                                 '0';
      block_interfaces(i).port_b_write_enable <= '1' when ((to_integer(port_b_block) = i) and
                                                           ((port_b_request.operator = Element_Write) or
                                                            (port_b_request.operator = Element_Read_Write))) else 
                                                 '0';
  end generate;

  port_a_flat_index <= resize(port_a_request.index.row * to_unsigned(description.n_columns, flat_index_length) +
                              port_a_request.index.column, flat_index_length);
  port_b_flat_index <= resize(port_b_request.index.row * to_unsigned(description.n_columns, flat_index_length) +
                              port_b_request.index.column, flat_index_length);

  port_a_block       <= resize(port_a_flat_index  /  (block_ram_segment_size / bitwidth), port_a_block'length);
  port_a_block_index <= resize(port_a_flat_index mod (block_ram_segment_size / bitwidth), port_a_block_index'length);
  port_b_block       <= resize(port_b_flat_index  /  (block_ram_segment_size / bitwidth), port_b_block'length);
  port_b_block_index <= resize(port_b_flat_index mod (block_ram_segment_size / bitwidth), port_b_block_index'length);

  shared_interface.port_a_addr(13 downto addressing_base)  <= Std_Logic_Vector(port_a_block_index);
  shared_interface.port_a_data_in((bitwidth - 1) downto 0) <= Std_Logic_Vector(port_a_data_in);
  shared_interface.port_b_addr(13 downto addressing_base)  <= Std_Logic_Vector(port_b_block_index);
  shared_interface.port_b_data_in((bitwidth - 1) downto 0) <= Std_Logic_Vector(port_b_data_in);

  port_a_data_out <= resize(Unsigned(block_interfaces(to_integer(port_a_block)).port_a_data_out), bitwidth);
  port_b_data_out <= resize(Unsigned(block_interfaces(to_integer(port_b_block)).port_b_data_out), bitwidth);

end architecture Dataflow;