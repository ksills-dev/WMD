----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/30/2018 03:12:40 PM
-- Design Name: 
-- Module Name: binary_to_bcd - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity binary_to_bcd is
    
    Port ( 
        binary_in : in UNSIGNED (15 downto 0);
        bcd_out :   out UNSIGNED (19 downto 0)
    );

end binary_to_bcd;

architecture Behavioral of binary_to_bcd is
begin
    process(binary_in)
    variable binary_temp: UNSIGNED (15 downto 0);
    variable bcd_temp: UNSIGNED (19 downto 0);
    begin   
 
        binary_temp := binary_in;
        bcd_temp    := (others => '0');

        --Double dabble

        for I in 0 to 15 loop

            if bcd_temp (15 downto 12) > 4 then
                bcd_temp (15 downto 12) := bcd_temp (15 downto 12) + 3;
            end if;

            if bcd_temp (11 downto 8) > 4 then
                bcd_temp (11 downto 8) := bcd_temp (11 downto 8) + 3;
            end if;

            if bcd_temp (7 downto 4) > 4 then
                bcd_temp (7 downto 4) := bcd_temp (7 downto 4) + 3;
            end if;
            
            if bcd_temp (3 downto 0) > 4 then
                bcd_temp (3 downto 0) := bcd_temp (3 downto 0) + 3;
            end if;
            
            bcd_temp := bcd_temp (18 downto 0) & binary_temp (15);
            binary_temp := binary_temp sll 1;

        end loop;

        bcd_out <= bcd_temp;

    end process;
end Behavioral;
